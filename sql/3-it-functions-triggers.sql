﻿
-- SELECTS PARA FUNCTIONS

-- - Retorne os meus 3 amigos mais publicadores de cada grupo (em termos do número de posts) no período;
SELECT id_us AS id_amg, id_grupo AS id_g, total_posts 
FROM 
(SELECT ROW_NUMBER() OVER (PARTITION BY g.id_grupo ORDER BY g.id_grupo) AS r,p.id_us,
	g.id_grupo, COUNT(*) as total_posts
	FROM(
	-- Pegar os amigos
		(((SELECT e.id_us AS id_amigo 
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = 23 AND confirm=true)
		UNION
		(SELECT e.id_us_amg
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = 23 AND confirm=true))
		EXCEPT
		(SELECT u.id FROM usuario u WHERE id = 23)
		) AS a
		
JOIN posts p ON (a.id_amigo = p.id_us) JOIN grupo g ON (p.id_us = g.id_us_part))
WHERE p.data_publicacao >= now() - interval '1 week' AND g.id_us_dono = 23 
GROUP BY p.id_us, g.id_grupo
ORDER BY total_posts DESC) AS result
WHERE result.r < 4


-- - Retorne os meus 10 amigos mais influentes 
       --(mais curtidos e comentados, sendo que 1 comentário vale por 5 curtições) no período;

SELECT p.id_us, (COUNT(data_curtir) + (5*COUNT(id_com))) AS ifl_pts FROM (
		(SELECT id FROM usuario)
		EXCEPT
		(SELECT e.id_us AS id_amigo 
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = 23 AND confirm=true)
		UNION
		(SELECT e.id_us_amg
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = 23 AND confirm=true)
		EXCEPT
		(SELECT u.id FROM usuario u WHERE id = 23)) AS amgs

		JOIN posts p ON (amgs.id = p.id_us) 
		LEFT OUTER JOIN comentario co ON (p.id_post=co.id_post_com)
		LEFT OUTER JOIN curtir c ON (p.id_post = c.id_post)
		WHERE c.data_curtir <= now() + interval '1 week' OR -- OR ou AND - DUVIDA
			co.data <= now() + interval '1 week'
		GROUP BY p.id_us
		ORDER BY ifl_pts DESC
		LIMIT 10;



--- Retorne os 3 posts de amigos meus mais influentes (mais curtidos e comentados, sendo 
	-- que 1 comentário vale por 5 curtições) por dia no período (i.e. 3 posts por dia);

SELECT ifl_pts, id_post FROM(
SELECT ROW_NUMBER() OVER (PARTITION BY tab1.data_publicacao ORDER BY tab1.ifl_pts DESC) AS r, ifl_pts, id_post
FROM (
	SELECT p.id_post, (COUNT(data_curtir) + (5*COUNT(id_com))) AS ifl_pts , p.data_publicacao FROM 

		(((SELECT e.id_us AS id_amigo 
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = 23 AND confirm=true)
		UNION
		(SELECT e.id_us_amg
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = 23 AND confirm=true))
		EXCEPT
		(SELECT u.id FROM usuario u WHERE id = 23)
		) AS amgs

		JOIN posts p ON (amgs.id_amigo = p.id_us) 
		LEFT OUTER JOIN comentario co ON (p.id_post=co.id_post_com)
		LEFT OUTER JOIN curtir c ON (p.id_post = c.id_post)
		WHERE c.data_curtir <= now() + interval '1 week' OR -- OR ou AND - DUVIDA
			co.data <= now() + interval '1 week'
		GROUP BY p.id_post) AS tab1) AS tab3
	WHERE tab3.r < 4;

-- - Retorne a taxa de influência de cada post meu no período, em ordem decrescente de influência, 
	-- a média de influência dos meus posts no período e a média de influência de todos os meus posts;


	-- IFL/POST/PERIODO
	SELECT * FROM
		(SELECT  p.id_post, COUNT(id_us_curtiu) + 5*COUNT(id_com) AS ifl_pts
		--SELECT *
		FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
			LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
		WHERE p.id_us = 23 
			AND p.data_publicacao BETWEEN now() - INTERVAL '1 week' AND now()
		GROUP BY p.id_post) as res
	ORDER BY ifl_pts DESC;


	-- MEDIA/PERIODO
	SELECT ifl_pts/qtd_post::DOUBLE PRECISION AS media_ifl 
	FROM(
		SELECT COUNT(id_us_curtiu) + 5*COUNT(id_com) AS ifl_pts, COUNT(DISTINCT p.id_post) AS qtd_post
		 --SELECT *
		FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
			LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
		WHERE p.id_us = 23 AND p.data_publicacao BETWEEN now() - INTERVAL '1 week' AND now()
	) AS result


	-- MEDIA TOTAL
	SELECT ifl_pts/qtd_post::DOUBLE PRECISION AS media_ifl 
	FROM(
		SELECT COUNT(id_us_curtiu) + 5*COUNT(id_com) AS ifl_pts, COUNT(DISTINCT p.id_post) AS qtd_post
		 --SELECT *
		FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
			LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
		WHERE p.id_us = 23 
	) AS result

-- - Retorne o comportamento de um amigo meu selecionado, por dia no período, em termos de: número de posts enviados, número total de curtições, 
	--número total de curtições em posts meus, número total de comentários, número total de comentários em posts meus;

-- TOTAL: POSTS, COMENTARIOS, CURTIR/DIA/PERIODO
SELECT COUNT( DISTINCT p.id_post ) as t_posts, COUNT( DISTINCT id_com) AS t_com, 
	SUM( CASE WHEN c.id_us_curtiu = 23 THEN 1 ELSE 0 END) as t_curtir, p.data_publicacao
FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
			LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
WHERE (p.id_us = 23 OR co.id_us = 23) AND p.data_publicacao BETWEEN now() - INTERVAL '2 week' AND now()
GROUP BY p.data_publicacao;




-- CURTIR E COMENTARIOS nos meus posts
SELECT * FROM(
	(SELECT  SUM( CASE WHEN id_us_curtiu = 28 THEN 1 ELSE 0 END) as t_curtir , data_publicacao
	FROM (
	(SELECT DISTINCT c.id_us_curtiu, p.data_publicacao -- SELECT *
	FROM posts p  LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
	WHERE ( c.id_us_curtiu = 28) AND p.id_us = 23 AND 
	(p.data_publicacao BETWEEN now()- INTERVAL '1 week' AND now() )
	) ) AS a GROUP BY data_publicacao ) AS tab_curtir

	CROSS JOIN
	
	(SELECT  SUM( CASE WHEN id_us = 28 THEN 1 ELSE 0 END) as t_com, data_publicacao
	FROM (
	SELECT DISTINCT co.id_us, p.data_publicacao -- SELECT *
	FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
	WHERE ( co.id_us = 28) AND p.id_us = 23 AND
	(p.data_publicacao BETWEEN now()- INTERVAL '1 week' AND now() ) 
	) AS b  GROUP BY data_publicacao )AS tab_com
)



-- FUNCTION's
-- FUNCAO QUE RETORNA OS TRES MAIS PUBLICADORES EM INTERVALO DE TEMPO
-- DROP FUNCTION get_top3_posters_f(INT,DATE, DATE)
CREATE OR REPLACE FUNCTION get_top3_posters_f(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_am INT, id_grupo INT, total_p BIGINT )  AS $$

		SELECT id_us AS id_amg, id_grupo AS id_g, total_posts 
		FROM 
		(SELECT ROW_NUMBER() OVER (PARTITION BY g.id_grupo ORDER BY COUNT(*) DESC) AS r,p.id_us,
			g.id_grupo, COUNT(*) as total_posts
			FROM(
			-- Pegar os amigos
				(((SELECT e.id_us AS id_amigo 
				FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
				WHERE u.id = 23 AND confirm=true)
				UNION
				(SELECT e.id_us_amg
				FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
				WHERE u.id = 23 AND confirm=true))
				EXCEPT
				(SELECT u.id FROM usuario u WHERE id = 23)
				ORDER BY id_amigo) AS a
				
		JOIN posts p ON (a.id_amigo = p.id_us) JOIN grupo g ON (p.id_us = g.id_us_part))
		WHERE (p.data_publicacao BETWEEN $2 AND $3) AND g.id_us_dono = 23 
		GROUP BY p.id_us, g.id_grupo
		ORDER BY total_posts DESC) AS result
		WHERE result.r < 4;
$$ LANGUAGE SQL;
SELECT * FROM get_top3_posters_f(23)






-- FUNCAO que retorna TOP 10 Influentes
CREATE OR REPLACE FUNCTION top10_infl_f(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_am INT, ifl_pts BIGINT )  AS $$

	SELECT p.id_us, (COUNT(data_curtir) + (5*COUNT(id_com))) AS ifl_pts FROM (
	
		(SELECT e.id_us AS id_amigo 
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = $1 AND confirm=true)
		UNION
		(SELECT e.id_us_amg
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = $1 AND confirm=true)
		EXCEPT
		(SELECT u.id FROM usuario u WHERE id = $1)) AS amgs

		JOIN posts p ON (amgs.id_amigo = p.id_us) 
		LEFT OUTER JOIN comentario co ON (p.id_post=co.id_post_com)
		LEFT OUTER JOIN curtir c ON (p.id_post = c.id_post)
	WHERE c.data_curtir BETWEEN $2 AND $3 OR
		co.data BETWEEN $2 AND $3
	GROUP BY p.id_us
		ORDER BY ifl_pts DESC
		LIMIT 10;
		
$$ LANGUAGE SQL;






-- FUNCAO que retorna TOP10 nao-amigos mais influentes
DROP FUNCTION top10_infl_f(INT, INTERVAL)

CREATE OR REPLACE FUNCTION top10_infl_nf(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_am INT, ifl_pts BIGINT )  AS $$

SELECT p.id_us, (COUNT(data_curtir) + (5*COUNT(id_com))) AS ifl_pts FROM (
		(SELECT id FROM usuario)
		EXCEPT
		(SELECT e.id_us AS id_amigo 
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = $1 AND confirm=true)
		UNION
		(SELECT e.id_us_amg
		FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
		WHERE u.id = $1 AND confirm=true)
		EXCEPT
		(SELECT u.id FROM usuario u WHERE id = $1)) AS amgs

		JOIN posts p ON (amgs.id = p.id_us) 
		LEFT OUTER JOIN comentario co ON (p.id_post=co.id_post_com)
		LEFT OUTER JOIN curtir c ON (p.id_post = c.id_post)
		WHERE (c.data_curtir BETWEEN $2 AND $3) OR -- OR ou AND - DUVIDA
			(co.data BETWEEN  $2 AND $3 )
		GROUP BY p.id_us
		ORDER BY ifl_pts DESC
		LIMIT 10;

$$ LANGUAGE SQL;



-- FUNCAO que retorna 3 posts mais influentes
CREATE OR REPLACE FUNCTION top3_posts_infl_f(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_post INT, ifl_pts BIGINT )  AS $$

SELECT id_post, ifl_pts FROM(
SELECT ROW_NUMBER() OVER (PARTITION BY tab1.data_publicacao ORDER BY tab1.ifl_pts DESC) AS r, ifl_pts, id_post
FROM (
  SELECT p.id_post, (COUNT(data_curtir) + (5*COUNT(id_com))) AS ifl_pts , p.data_publicacao FROM 

    (((SELECT e.id_us AS id_amigo 
    FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
    WHERE u.id = $1 AND confirm=true)
    UNION
    (SELECT e.id_us_amg
    FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
    WHERE u.id = $1 AND confirm=true))
    EXCEPT
    (SELECT u.id FROM usuario u WHERE id = $1)
    ) AS amgs

    JOIN posts p ON (amgs.id_amigo = p.id_us) 
    LEFT OUTER JOIN comentario co ON (p.id_post=co.id_post_com)
    LEFT OUTER JOIN curtir c ON (p.id_post = c.id_post)
    WHERE (c.data_curtir BETWEEN $2 AND $3) OR 
      (co.data BETWEEN $2 AND $3)
    GROUP BY p.id_post) AS tab1) AS tab3
  WHERE tab3.r < 4;

$$ LANGUAGE SQL;






--TESTE FUNCAO
SELECT * FROM get_top3_posters_f(23); -- TOP 3 POSTERS
SELECT * FROM top10_infl_f(23);       -- TOP 10 Influentes
SELECT * FROM top10_infl_f(23);       -- TOP 10 Influentes (nao-amigos)
SELECT * FROM top3_posts_infl_nf(23)  -- TOP 3 Posts Influentes (nao-amigos)







--SELECT's testes 
SELECT * 
FROM posts 
WHERE data_publicacao > now() + interval '1 week';


--INSERT's
INSERT INTO posts (id_us, conteudo) VALUES (27, 'ola9');
INSERT INTO grupo (id_grupo, id_us_dono, id_us_part, nome_grupo) 
	VALUES (2, 23, 26, 'teste1'),
	(2, 23, 25, 'teste1'),
	(2, 23, 27, 'teste1'),
	(2, 23, 30, 'teste1');
	VALUES (2, 23, 26, 'teste1'),
	VALUES (2, 23, 26, 'teste1'),
	VALUES (2, 23, 26, 'teste1'),



FROM curtir c JOIN posts p ON (c.id_post = p.id_post);


--INSERT's
INSERT INTO posts (id_us, conteudo) VALUES (32, 'Merda Bosta FDP Idiota');

INSERT INTO grupo (id_grupo, id_us_dono, id_us_part, nome_grupo) 
	VALUES (2, 23, 26, 'teste1'),
	(2, 23, 25, 'teste1'),
	(2, 23, 27, 'teste1'),
	(2, 23, 30, 'teste1');
	VALUES (2, 23, 26, 'teste1'),
	VALUES (2, 23, 26, 'teste1'),
	VALUES (2, 23, 26, 'teste1'),



INSERT INTO comentario (id_post_com, conteudo_com, data, id_us) VALUES
	(16, 'Que legal!', now(), 28)
	
INSERT INTO curtir (id_post, id_us_curtiu, data_curtir) VALUES
	( 16, , now() )

	SELECT * FROM curtir c JOIN posts p ON (c.id_post = p.id_post)


-- TRIGGER HistPost
CREATE OR REPLACE FUNCTION guardar_hist_post()
RETURNS TRIGGER AS $$
	BEGIN
		IF tg_op = 'UPDATE' OR tg_op = 'DELETE' THEN
			INSERT INTO historico_posts (id_post, conteudo, data_mod) 
				VALUES (OLD.id_post, OLD.conteudo, now());
			RETURN NEW;
		END IF;
	END;
$$ LANGUAGE plpgsql;



CREATE TRIGGER "historico_post_on_update" AFTER UPDATE ON posts FOR EACH ROW 

EXECUTE PROCEDURE guardar_hist_post();

CREATE TRIGGER "historico_post_on_delete" AFTER UPDATE ON posts FOR EACH ROW 
EXECUTE PROCEDURE guardar_hist_post();

-- DROP TRIGGER "historico_post_on_delete" ON posts

SELECT * FROM historico_posts
SELECT * FROM posts


-- FUNCTION Conteudo Ofensivo

CREATE OR REPLACE FUNCTION reportar_cont_ofsv( parametro VARCHAR(100) )
RETURNS VOID AS $$

	BEGIN
		INSERT INTO log_conteudo_ofensivo (id_post, conteudo, data_publicacao, id_us_post, nome, reincidente)
			(  SELECT DISTINCT p.id_post,  p.conteudo, p.data_publicacao, p.id_us AS id_us_post, u.nome,

				CASE WHEN EXISTS ( SELECT id_us_post 
					FROM log_conteudo_ofensivo 
					WHERE id_us_post = u.id
					)
				THEN TRUE
				ELSE FALSE
				END
				
			   FROM posts p JOIN usuario u ON (p.id_us = u.id)
			   WHERE (p.conteudo ~* lower('.*'||lower(parametro)||'.*'))
				);
	END;
$$ LANGUAGE plpgsql;


SELECT reportar_cont_ofsv('FDP');

SELECT * FROM log_conteudo_ofensivo



 -- DROP TABLE IF EXISTS log_conteudo_ofensivo;
CREATE TABLE log_conteudo_ofensivo(
  id_log SERIAL PRIMARY KEY,
  id_post INT,
  conteudo TEXT,
  data_publicacao DATE,
  id_us_post INT,
  nome VARCHAR(250),
  reincidente BOOLEAN
  
);

-- Visualizar tabelas
SELECT * FROM comentario;
SELECT * FROM curtir;
SELECT * FROM e_amigo;
SELECT * FROM grupo;
SELECT * FROM post_multimidia;
SELECT * FROM posts;
SELECT * FROM usuario;



SELECT * FROM posts, curtir, comentario;

