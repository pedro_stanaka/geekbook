
/*
 *
 *  1 Ciclo
 *  
 *
*/


CREATE TABLE usuario
(
  id serial NOT NULL,
  nome character varying(255),
  email character varying(200),
  sexo character(1),
  datanasc date,
  login character varying(200) NOT NULL,
  senha character varying(200) NOT NULL,
  avatar character varying NOT NULL DEFAULT 'img/def_avatar.jpg'::character varying,
  CONSTRAINT usuario_pkey PRIMARY KEY (id ),
  CONSTRAINT usuario_login_key UNIQUE (login )
);


/*
 *
 *  2 Ciclo
 *  
 *
*/

CREATE TABLE e_amigo
(
  id_us integer NOT NULL,
  id_us_amg integer NOT NULL,
  confirm boolean DEFAULT false,
  CONSTRAINT e_amigo_pkey PRIMARY KEY (id_us , id_us_amg ),
  CONSTRAINT e_amigo_id_us_amg_fkey FOREIGN KEY (id_us_amg)
      REFERENCES usuario (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT e_amigo_id_us_fkey FOREIGN KEY (id_us)
      REFERENCES usuario (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
);

CREATE TABLE grupo
(
  id_grupo integer NOT NULL,
  id_us_dono integer NOT NULL,
  id_us_part integer NOT NULL,
  nome_grupo character varying,
  CONSTRAINT grupo_pkey PRIMARY KEY (id_grupo , id_us_dono , id_us_part ),
  CONSTRAINT grupo_id_dono_fkey FOREIGN KEY (id_us_dono)
      REFERENCES usuario (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT grupo_id_us_part_fkey FOREIGN KEY (id_us_part)
      REFERENCES usuario (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL
);

/*
 *
 *  3 Ciclo
 *  
 *
*/


  CREATE TABLE posts(
    id_post serial,
    id_us integer,
    conteudo TEXT,
    data_publicacao DATE DEFAULT current_date,

    CONSTRAINT posts_pk PRIMARY KEY (id_post),

    CONSTRAINT post_usuario_fk FOREIGN KEY (id_us) REFERENCES usuario(id) 
        ON DELETE CASCADE ON UPDATE CASCADE
  );

  CREATE TABLE post_multimidia( -- Relação que guarda as informações de imagens gravadas em posts
    id_post integer,
    imagem  varchar(200),

    CONSTRAINT post_multimidia_pk PRIMARY KEY (id_post, imagem),
    CONSTRAINT post_mul_posts_fk FOREIGN KEY (id_post) REFERENCES posts (id_post)
      ON DELETE CASCADE ON UPDATE CASCADE
  );

  CREATE TABLE comentario(
    id_com serial,
    id_us integer,
    id_post_com integer,
    conteudo_com TEXT,

    CONSTRAINT pk_comentario PRIMARY KEY (id_com)
    CONSTRAINT com_usu_fk FOREIGN KEY (id_us) REFERENCES usuario(id)
      ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT com_post_fk FOREIGN KEY (id_post_com) REFERENCES posts (id_post)
      ON UPDATE CASCADE ON DELETE CASCADE 
      
  );


  CREATE TABLE curtir
  (
    id_post integer,
    id_us_curtiu integer,
    CONSTRAINT curtir_pk PRIMARY KEY (id_post , id_us_curtiu ),
    CONSTRAINT curtir_posts_fk FOREIGN KEY (id_post) REFERENCES posts (id_post)
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT curtir_usu_fk FOREIGN KEY (id_us_curtiu) REFERENCES usuario (id)
        ON UPDATE CASCADE ON DELETE CASCADE
  );


ALTER TABLE curtir ADD COLUMN data DATE NOT NULL DEFAULT today;

ALTER TABLE comentario ADD COLUMN data DATE NOT NULL DEFAULT today;

ALTER TABLE curtir ALTER COLUMN data_curtir SET DEFAULT now();

ALTER TABLE comentario ALTER COLUMN data SET DEFAULT now();

ALTER TABLE posts ALTER COLUMN data_publicacao SET DEFAULT now();
 


-- TABELA PARA TRIGGER E FUNCTION
CREATE TABLE historico_posts (
      id_his SERIAL,
      id_post INT,
      conteudo TEXT,
      data_mod DATE,

      CONSTRAINT historico_posts_pk PRIMARY KEY (id_his),
      CONSTRAINT historico_posts_fk FOREIGN KEY (id_post) REFERENCES posts(id_post)
        ON DELETE NO ACTION ON UPDATE CASCADE

);


CREATE TABLE log_conteudo_ofensivo(
  id_log SERIAL PRIMARY KEY,
  id_post INT,
  conteudo TEXT,
  data_publicacao DATE,
  id_us_post INT,
  nome VARCHAR(250),
  reincidente BOOLEAN
  
);




/*
 *
 *  3 Ciclo - #SECAO FUNCTIONS#
 *  
 *
*/
/*
  Todas as funcoes de estatistica
  get_top3_posters_f(INT, DATE, DATE)
  comportamento_curtir_com_meus_posts(INT, DATE, DATE)
  comportamento_total_post_com_curtir(INT, DATE, DATE)
  infl_todos_posts(INT, DATE, DATE)
  media_infl_posts(INT)
  media_infl_todos_posts()
  top10_infl_f(INT, DATE, DATE)
  top10_infl_nf(INT, DATE, DATE)
  top3_posts_infl_nf(INT, DATE, DATE)
*/


-- FUNCAO QUE RETORNA OS TRES MAIS PUBLICADORES EM INTERVALO DE TEMPO
CREATE OR REPLACE FUNCTION get_top3_posters_f(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_am INT, id_grupo INT, total_p BIGINT )  AS $$

    SELECT id_us AS id_amg, id_grupo AS id_g, total_posts 
    FROM 
    (SELECT ROW_NUMBER() OVER (PARTITION BY g.id_grupo ORDER BY COUNT(*) DESC) AS r,p.id_us,
      g.id_grupo, COUNT(*) as total_posts
      FROM(
      -- Pegar os amigos
        (((SELECT e.id_us AS id_amigo 
        FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
        WHERE u.id = 23 AND confirm=true)
        UNION
        (SELECT e.id_us_amg
        FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
        WHERE u.id = 23 AND confirm=true))
        EXCEPT
        (SELECT u.id FROM usuario u WHERE id = 23)
        ORDER BY id_amigo) AS a
        
    JOIN posts p ON (a.id_amigo = p.id_us) JOIN grupo g ON (p.id_us = g.id_us_part))
    WHERE (p.data_publicacao BETWEEN $2 AND $3) AND g.id_us_dono = 23 
    GROUP BY p.id_us, g.id_grupo
    ORDER BY total_posts DESC) AS result
    WHERE result.r < 4;
$$ LANGUAGE SQL;

SELECT * FROM get_top3_posters_f(23)

-- TOP 10 Influentes - AMIGOS


CREATE OR REPLACE FUNCTION top10_infl_f(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_am INT, ifl_pts BIGINT )  AS $$

  SELECT p.id_us, (COUNT(data_curtir) + (5*COUNT(id_com))) AS ifl_pts FROM (
  
    (SELECT e.id_us AS id_amigo 
    FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
    WHERE u.id = $1 AND confirm=true)
    UNION
    (SELECT e.id_us_amg
    FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
    WHERE u.id = $1 AND confirm=true)
    EXCEPT
    (SELECT u.id FROM usuario u WHERE id = $1)) AS amgs

    JOIN posts p ON (amgs.id_amigo = p.id_us) 
    LEFT OUTER JOIN comentario co ON (p.id_post=co.id_post_com)
    LEFT OUTER JOIN curtir c ON (p.id_post = c.id_post)
  WHERE c.data_curtir BETWEEN $2 AND $3 OR
    co.data BETWEEN $2 AND $3
  GROUP BY p.id_us
    ORDER BY ifl_pts DESC
    LIMIT 10;
    
$$ LANGUAGE SQL;

-- TOP 10 Influentes - Não amigos


CREATE OR REPLACE FUNCTION top10_infl_nf(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_am INT, ifl_pts BIGINT )  AS $$

SELECT p.id_us, (COUNT(data_curtir) + (5*COUNT(id_com))) AS ifl_pts FROM (
    (SELECT id FROM usuario)
    EXCEPT
    (SELECT e.id_us AS id_amigo 
    FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
    WHERE u.id = $1 AND confirm=true)
    UNION
    (SELECT e.id_us_amg
    FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
    WHERE u.id = $1 AND confirm=true)
    EXCEPT
    (SELECT u.id FROM usuario u WHERE id = $1)) AS amgs

    JOIN posts p ON (amgs.id = p.id_us) 
    LEFT OUTER JOIN comentario co ON (p.id_post=co.id_post_com)
    LEFT OUTER JOIN curtir c ON (p.id_post = c.id_post)
    WHERE (c.data_curtir BETWEEN $2 AND $3) OR -- OR ou AND - DUVIDA
      (co.data BETWEEN  $2 AND $3 )
    GROUP BY p.id_us
    ORDER BY ifl_pts DESC
    LIMIT 10;

$$ LANGUAGE SQL;





-- TOP 3 POSTS MAIS INFLUENTES DE AMIGOS
CREATE OR REPLACE FUNCTION top3_posts_infl_nf(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_post INT, ifl_pts BIGINT )  AS $$

SELECT id_post, ifl_pts FROM(
SELECT ROW_NUMBER() OVER (PARTITION BY tab1.data_publicacao ORDER BY tab1.ifl_pts DESC) AS r, ifl_pts, id_post
FROM (
  SELECT p.id_post, (COUNT(data_curtir) + (5*COUNT(id_com))) AS ifl_pts , p.data_publicacao FROM 

    (((SELECT e.id_us AS id_amigo 
    FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
    WHERE u.id = $1 AND confirm=true)
    UNION
    (SELECT e.id_us_amg
    FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR u.id = e.id_us_amg)
    WHERE u.id = $1 AND confirm=true))
    EXCEPT
    (SELECT u.id FROM usuario u WHERE id = $1)
    ) AS amgs

    JOIN posts p ON (amgs.id_amigo = p.id_us) 
    LEFT OUTER JOIN comentario co ON (p.id_post=co.id_post_com)
    LEFT OUTER JOIN curtir c ON (p.id_post = c.id_post)
    WHERE (c.data_curtir BETWEEN $2 AND $3) OR 
      (co.data BETWEEN $2 AND $3)
    GROUP BY p.id_post) AS tab1) AS tab3
  WHERE tab3.r < 4;

$$ LANGUAGE SQL;


/**
*
*
* AS PROXIMAS 3 FAZEM '1' ESTATISTICA 
*/


-- INFLUENCIA DE TODOS OS POSTS NO PERIODO
CREATE OR REPLACE FUNCTION infl_todos_posts(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE ( id_post INT, ifl_pts BIGINT )  AS $$

    SELECT * FROM
        (SELECT  p.id_post, COUNT(id_us_curtiu) + 5*COUNT(id_com) AS ifl_pts
        FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
          LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
        WHERE p.id_us = $1 
          AND p.data_publicacao BETWEEN $2 AND $3
        GROUP BY p.id_post) as res
    ORDER BY ifl_pts DESC;

$$ LANGUAGE SQL;


-- MEDIA DE INFLUENCIA DE TODOS OS POSTS NO PERIODO
CREATE OR REPLACE FUNCTION media_infl_todos_posts(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS DOUBLE PRECISION  AS $$

    SELECT ifl_pts/qtd_post::DOUBLE PRECISION AS media_ifl 
      FROM(
        SELECT COUNT(id_us_curtiu) + 5*COUNT(id_com) AS ifl_pts, COUNT(DISTINCT p.id_post) AS qtd_post
        FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
          LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
        WHERE p.id_us = $1 AND p.data_publicacao BETWEEN $2 AND $3
      ) AS result

$$ LANGUAGE SQL;


-- MEDIA TOTAL

CREATE OR REPLACE FUNCTION media_infl_posts(id_us INT)
RETURNS DOUBLE PRECISION  AS $$

  SELECT ifl_pts/qtd_post::DOUBLE PRECISION AS media_ifl 
  FROM(
    SELECT COUNT(id_us_curtiu) + 5*COUNT(id_com) AS ifl_pts, COUNT(DISTINCT p.id_post) AS qtd_post
    FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
      LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
    WHERE p.id_us = $1 
  ) AS result

$$ LANGUAGE SQL;



  /*
  *
  *  Retorne o comportamento de um amigo meu selecionado, por dia no período, em termos de: número de posts enviados, número total de curtições, 
  *  número total de curtições em posts meus, número total de comentários, número total de comentários em posts meus;
  *  
  *
  */



-- TOTAL: POSTS, COMENTARIOS, CURTIR/DIA/PERIODO

CREATE OR REPLACE FUNCTION comportamento_total_post_com_curtir(id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
  RETURNS TABLE (t_posts BIGINT, t_com BIGINT) AS $$


    SELECT COUNT( DISTINCT p.id_post ) as t_posts, COUNT( DISTINCT id_com) AS t_com, 
      SUM( CASE WHEN c.id_us_curtiu = $1 THEN 1 ELSE 0 END) as t_curtir, p.data_publicacao
    FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
          LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
    WHERE (p.id_us = $1 OR co.id_us = $1) AND p.data_publicacao BETWEEN $2 AND $3
    GROUP BY p.data_publicacao;


$$ LANGUAGE SQL;


-- CURTIR e COMENTARIOS EM MEUS POSTS
CREATE OR REPLACE FUNCTION comportamento_curtir_com_meus_posts( id_amg INT, id_us INT, ini_p DATE = now() - INTERVAL '1 week', fim_p DATE = now())
RETURNS TABLE (t_curtir BIGINT, data_curtir DATE, t_com BIGINT, data_com DATE) AS $$
    SELECT * FROM(
      (SELECT  SUM( CASE WHEN id_us_curtiu = $1 THEN 1 ELSE 0 END) as t_curtir , data_publicacao
      FROM (
      (SELECT DISTINCT c.id_us_curtiu, p.data_publicacao -- SELECT *
      FROM posts p  LEFT OUTER JOIN curtir c ON ( p.id_post = c.id_post )
      WHERE ( c.id_us_curtiu = $1) AND p.id_us = $2 AND 
      (p.data_publicacao BETWEEN $3 AND $4 )
      ) ) AS a GROUP BY data_publicacao ) AS tab_curtir

      CROSS JOIN
      
      (SELECT  SUM( CASE WHEN id_us = $1 THEN 1 ELSE 0 END) as t_com, data_publicacao
      FROM (
      SELECT DISTINCT co.id_us, p.data_publicacao -- SELECT *
      FROM posts p  LEFT OUTER JOIN comentario co ON ( p.id_post = co.id_post_com ) 
      WHERE ( co.id_us = $1) AND p.id_us = $2 AND
      (p.data_publicacao BETWEEN $3 AND $4 ) 
      ) AS b  GROUP BY data_publicacao )AS tab_com
    )

$$ LANGUAGE SQL;



-- TRIGGER HistPost
CREATE OR REPLACE FUNCTION guardar_hist_post()
RETURNS TRIGGER AS $$
  BEGIN
    IF tg_op = 'UPDATE' OR tg_op = 'DELETE' THEN
      INSERT INTO historico_posts (id_post, conteudo, data_mod) 
        VALUES (OLD.id_post, OLD.conteudo, now());
      RETURN NEW;
    END IF;
  END;
$$ LANGUAGE plpgsql;



CREATE TRIGGER "historico_post_on_update" AFTER UPDATE ON posts FOR EACH ROW 

EXECUTE PROCEDURE guardar_hist_post();

CREATE TRIGGER "historico_post_on_delete" AFTER UPDATE ON posts FOR EACH ROW 
EXECUTE PROCEDURE guardar_hist_post();

-- DROP TRIGGER "historico_post_on_delete" ON posts

SELECT * FROM historico_posts
SELECT * FROM posts


-- FUNCTION Conteudo Ofensivo

CREATE OR REPLACE FUNCTION reportar_cont_ofsv( parametro VARCHAR(100) )
RETURNS VOID AS $$

  BEGIN
    INSERT INTO log_conteudo_ofensivo (id_post, conteudo, data_publicacao, id_us_post, nome, reincidente)
      (  SELECT DISTINCT p.id_post,  p.conteudo, p.data_publicacao, p.id_us AS id_us_post, u.nome,

        CASE WHEN EXISTS ( SELECT id_us_post 
          FROM log_conteudo_ofensivo 
          WHERE id_us_post = u.id
          )
        THEN TRUE
        ELSE FALSE
        END
        
         FROM posts p JOIN usuario u ON (p.id_us = u.id)
         WHERE (p.conteudo ~* lower('.*'||lower($1)||'.*'))
        );
  END;
$$ LANGUAGE plpgsql;


SELECT reportar_cont_ofsv('FDP');

SELECT * FROM log_conteudo_ofensivo


