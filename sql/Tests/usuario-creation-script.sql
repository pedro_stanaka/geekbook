CREATE TABLE usuario
(
  id SERIAL NOT NULL,
  nome VARCHAR(255),
  email VARCHAR(200),
  sexo CHAR(1),
  datanasc DATE,
  login VARCHAR(200) NOT NULL,
  senha VARCHAR(200) NOT NULL,
  CONSTRAINT PRIMARY KEY (id ),
  UNIQUE (login)
);

ALTER TABLE usuario ADD COLUMN avatar VARCHAR NOT NULL DEFAULT 'img/def_avatar.jpg';



create table e_amigo(
    id_us INT,
    id_us_amg INT,

    FOREIGN KEY (id_us) REFERENCES usuario(id) 
	ON UPDATE RESTRICT ON DELETE RESTRICT,
    FOREIGN KEY (id_us_amg) REFERENCES usuario(id)
	ON UPDATE RESTRICT ON DELETE RESTRICT,
    PRIMARY KEY (id_us, id_us_amg)
);

CREATE TABLE grupo(
   id_grupo INT,
   id_us_dono INT,
   id_us_part INT,
   nome_grupo VARCHAR,
   FOREIGN KEY (id_us_dono) REFERENCES usuario(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
   FOREIGN KEY (id_us_part) REFERENCES usuario(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
   PRIMARY KEY (id_grupo, id_us_dono, id_us_part)
);

create table req_amizade(--requisição de nova amizade

   id_req INT,
   id_us_requerente INT,
   id_us_requerido INT,

   FOREIGN KEY (id_us_requerente) REFERENCES usuario(id) 
	ON UPDATE CASCADE ON DELETE CASCADE,
   FOREIGN KEY (id_us_requerido) REFERENCES usuario(id) 
	ON UPDATE CASCADE ON DELETE CASCADE,
   PRIMARY KEY (id_req, id_us_requerente, id_us_requerido) 

);

ALTER TABLE e_amigo ADD COLUMN confirm BOOLEAN DEFAULT FALSE;

DROP TABLE req_amizade;




