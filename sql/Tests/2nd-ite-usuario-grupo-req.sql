
CREATE TABLE usuario (
  id_usuario INT,
  nome VARCHAR NOT NULL,
  endereco VARCHAR NULL DEFAULT 'Não Informado',
  email VARCHAR NOT NULL,
  login VARCHAR NOT NULL,
  senha VARCHAR NOT NULL,
  foto VARCHAR NOT NULL DEFAULT 'img/def_avatar.jpg',
  PRIMARY KEY(id_usuario),
  INDEX Usuario_FKIndex1(usuario_id_usuario)
);

CREATE TABLE grupo (
  id_grupo INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  usuario_id_usuario INTEGER UNSIGNED NOT NULL,
  nome VARCHAR NOT NULL,
  data_criacao DATE NOT NULL DEFAULT 'TODAY',
  descricao TEXT NULL,
  PRIMARY KEY(id_grupo, usuario_id_usuario),
  INDEX grupo_FKIndex1(usuario_id_usuario)
);
