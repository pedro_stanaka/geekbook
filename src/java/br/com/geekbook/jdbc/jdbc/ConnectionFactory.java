/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.geekbook.jdbc.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author pedrotanaka
 */
public class ConnectionFactory {
    public Connection getConnection(){
        try {
            return DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/javaDB", "javadb", "java"
                    );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
