/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author pedrotanaka
 */
public class ConnectionFactory {
    public Connection getConnection() throws ClassNotFoundException{
        try {
            Class.forName("org.postgresql.Driver");
            
            return DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/javadb", "pedrotanaka", "pedro123"
                    );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
