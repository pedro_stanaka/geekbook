package com.geekbook.jdbc.teste;

import com.geekbook.jdbc.ConnectionFactory;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author pedrotanaka
 */
public class TestaConexao {
    public static void main (String args[]) throws SQLException, ClassNotFoundException {
        Connection con = new ConnectionFactory().getConnection();
        System.out.println("Conexão Aberta com sucesso.");
        con.close();
    }
}
