/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.jdbc.teste;

import com.geekbook.jdbc.dao.UsuarioDAO;
import com.geekbook.jdbc.modelo.Usuario;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author pedrotanaka
 */
public class TestaLista {

    public static void main(String args[]) throws SQLException, ClassNotFoundException {
        UsuarioDAO usuario = new UsuarioDAO();
        List<Usuario> lu = usuario.getLista();

        for (Usuario usuario1 : lu) {
            System.out.println("Nome: " + usuario1.getNome());
            System.out.println("Email: " + usuario1.getEmail());
        }
    }
}
