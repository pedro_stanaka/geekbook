/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.jdbc.teste;

import com.geekbook.jdbc.dao.UsuarioDAO;
import com.geekbook.jdbc.modelo.Usuario;
import java.sql.SQLException;
import java.util.Calendar;

/**
 *
 * @author pedrotanaka
 */
public class TestaInsere {
    public static void main(String args[]) throws ClassNotFoundException, SQLException {
        
        Usuario user = new Usuario();
        user.setNome("Pedro");
        user.setEmail("pedro@gmail.com");
        user.setDataNascimento(Calendar.getInstance());
        
        UsuarioDAO userbd = new UsuarioDAO();
        userbd.adiciona(user);
        
        
        System.out.println("Gravado!");
    }
}
