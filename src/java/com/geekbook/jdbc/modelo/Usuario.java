
package com.geekbook.jdbc.modelo;

import com.geekbook.servlet.doCadastroUsuario;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pedrotanaka
 */
public class Usuario {

    private Long id;
    private String nome;
    private String email;
    private String sexo;
    private Calendar dataNascimento;
    private String login;
    private String senha;
    private String avatar;
    private List<Long> amigosId;
    private List<Long> requisitadosId;
    private List<Long> pedidosPendentes;
    private Long grupo;
    private int ifl_pts;
    private int post_cont;

    public boolean isAmigo(Long id) {
        
        if (amigosId != null) {
            if (amigosId.contains(id)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Fala se o usuário fez uma requisição para o idReq.
     * @param idReq
     * @return
     */
    public boolean isRequisitado(Long idReq){
        
        if(this.requisitadosId != null){
            if(requisitadosId.contains(idReq)){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Fala se o usuário com idReq fez pedido de amizade para esse
     * usuário.
     * @param idReq
     * @return
     */
    public boolean isPendente(Long idReq){
        
        if(this.pedidosPendentes != null){
            if(pedidosPendentes.contains(idReq)){
                return true;
            }
        }
        
        return false;
        
    }
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the dataNascimento
     */
    public Calendar getDataNascimento() {
        return dataNascimento;
    }
    
    /**
     *
     * @return
     */
    public String getDataNascimentoToString() {
        
        String data;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date dt = new Date(dataNascimento.getTimeInMillis());
        data = sdf.format(dt);
        
        return data;
    }

    /**
     * @param dataNascimento the dataNascimento to set
     */
    public void setDataNascimento(Calendar dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        String data = dataNascimento;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        
                
        try {
            cal.setTime(sdf.parse(data));
            this.dataNascimento = cal;
        } catch (ParseException pEx) {
            Logger.getLogger(doCadastroUsuario.class.getName()).log(Level.SEVERE, null, pEx);
        }
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(char sexo) {
        this.setSexo(sexo);
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar the avatar to set
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * @return the amigosId
     */
    public List<Long> getAmigosId() {
        return amigosId;
    }

    /**
     * @param amigosId the amigosId to set
     */
    public void setAmigosId(List<Long> amigosId) {
        this.amigosId = amigosId;
    }

    /**
     * @return the requisitadosId
     */
    public List<Long> getRequisitadosId() {
        return requisitadosId;
    }

    /**
     * @param requisitadosId the requisitadosId to set
     */
    public void setRequisitadosId(List<Long> requisitadosId) {
        this.requisitadosId = requisitadosId;
    }

    /**
     * @return the pedidosPendentes
     */
    public List<Long> getPedidosPendentes() {
        return pedidosPendentes;
    }

    /**
     * @param pedidosPendentes the pedidosPendentes to set
     */
    public void setPedidosPendentes(List<Long> pedidosPendentes) {
        this.pedidosPendentes = pedidosPendentes;
    }

    /**
     * @return the grupo
     */
    public Long getGrupo() {
        return grupo;
    }

    /**
     * @param grupo the grupo to set
     */
    public void setGrupo(Long grupo) {
        this.grupo = grupo;
    }

    /**
     * @return the ifl_pts
     */
    public int getIfl_pts() {
        return ifl_pts;
    }

    /**
     * @param ifl_pts the ifl_pts to set
     */
    public void setIfl_pts(int ifl_pts) {
        this.ifl_pts = ifl_pts;
    }

    /**
     * @return the post_cont
     */
    public int getPost_cont() {
        return post_cont;
    }

    /**
     * @param post_cont the post_cont to set
     */
    public void setPost_cont(int post_cont) {
        this.post_cont = post_cont;
    }

}
