/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.jdbc.modelo;

import java.util.Calendar;
import java.util.List;

/**
 *
 * @author pedrotanaka
 */
public class Post {
    private Long idPost;
    private String conteudo;
    private Long idUsuario;
    private boolean temAnexos;
    private List<String> anexos;
    private List<String> comentarios;
    private Calendar dataPublicacao;
    private int qtdCurtir;
    
    /**
     * @return the idPost
     */
    public Long getIdPost() {
        return idPost;
    }

    /**
     * @param idPost the idPost to set
     */
    public void setIdPost(Long idPost) {
        this.idPost = idPost;
    }

    /**
     * @return the conteudo
     */
    public String getConteudo() {
        return conteudo;
    }

    /**
     * @param conteudo the conteudo to set
     */
    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the temAnexos
     */
    public boolean isTemAnexos() {
        return temAnexos;
    }

    /**
     * @param temAnexos the temAnexos to set
     */
    public void setTemAnexos(boolean temAnexos) {
        this.temAnexos = temAnexos;
    }

    /**
     * @return the anexos
     */
    public List<String> getAnexos() {
        return anexos;
    }

    /**
     * @param anexos the anexos to set
     */
    public void setAnexos(List<String> anexos) {
        this.anexos = anexos;
    }

    /**
     * @return the dataPublicacao
     */
    public Calendar getDataPublicacao() {
        return dataPublicacao;
    }

    /**
     * @param dataPublicacao the dataPublicacao to set
     */
    public void setDataPublicacao(Calendar dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }
    
    
    /**
     * @param dataPublicacao the dataPublicacao to set
     */
    public void setDataPublicacao(java.sql.Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        this.dataPublicacao = cal;
    }

    /**
     * @return the qtdCurtir
     */
    public int getQtdCurtir() {
        return qtdCurtir;
    }

    /**
     * @param qtdCurtir the qtdCurtir to set
     */
    public void setQtdCurtir(int qtdCurtir) {
        this.qtdCurtir = qtdCurtir;
    }

    /**
     * @return the comentarios
     */
    public List<String> getComentarios() {
        return comentarios;
    }

    /**
     * @param comentarios the comentarios to set
     */
    public void setComentarios(List<String> comentarios) {
        this.comentarios = comentarios;
    }
    
}
