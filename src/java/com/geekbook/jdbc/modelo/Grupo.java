/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.jdbc.modelo;

/**
 *
 * @author pedrotanaka
 */
public class Grupo {
    private Long idGrupo;
    private String nomeGrupo;
    private Long idUsDono;

    /**
     * @return the idGrupo
     */
    public Long getIdGrupo() {
        return idGrupo;
    }

    /**
     * @param idGrupo the idGrupo to set
     */
    public void setIdGrupo(Long idGrupo) {
        this.idGrupo = idGrupo;
    }

    /**
     * @return the nomeGrupo
     */
    public String getNomeGrupo() {
        return nomeGrupo;
    }

    /**
     * @param nomeGrupo the nomeGrupo to set
     */
    public void setNomeGrupo(String nomeGrupo) {
        this.nomeGrupo = nomeGrupo;
    }

    /**
     * @return the idUsDono
     */
    public Long getIdUsDono() {
        return idUsDono;
    }

    /**
     * @param idUsDono the idUsDono to set
     */
    public void setIdUsDono(Long idUsDono) {
        this.idUsDono = idUsDono;
    }

}
