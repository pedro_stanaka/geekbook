/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.jdbc.dao;

import com.geekbook.jdbc.ConnectionFactory;
import com.geekbook.jdbc.modelo.Usuario;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pedrotanaka
 */
public class UsuarioDAO {

    private Connection conexao;

    public UsuarioDAO() throws ClassNotFoundException {
        this.conexao = new ConnectionFactory().getConnection();
    }
    

    /**
     *
     * @param usuario
     */
    public boolean excluir(Usuario usuario) {
        String query = "DELETE FROM usuario"
                + "WHERE id=? RETURNING id";
        PreparedStatement stmt;
        try {
            stmt = this.conexao.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            stmt.close();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }


    }

    /**
     *
     * @param usuario objeto com novos dados, e id setado para conferencia no
     * banco
     */
    public void editar(Usuario usuario) {
        String query = "UPDATE usuario "
                + "SET (nome, email, sexo, datanasc, login, senha, avatar) = "
                + "(?,?,?,?,?,md5(?),?)"
                + "WHERE id=?";
        PreparedStatement stmt;
        try {
            stmt = this.conexao.prepareStatement(query);
            stmt.setString(1, usuario.getNome());
            stmt.setString(2, usuario.getEmail());
            stmt.setString(3, usuario.getSexo());
            stmt.setDate(4, new Date(usuario.getDataNascimento().getTimeInMillis()));
            stmt.setString(5, usuario.getLogin());
            stmt.setString(6, usuario.getSenha());
            stmt.setString(7, usuario.getAvatar());
            stmt.setLong(8, usuario.getId());

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param id
     * @return Uma instancia de Usuario com os dados referentes em caso de
     * suceso, null caso não encontre o usuario com o id passado como parametro.
     * @throws SQLException
     */
    public Usuario getInstanceById(Long id) throws ClassNotFoundException, SQLException {
        String query =
                "SELECT * "
                + "FROM usuario u "
                + "WHERE u.id = ?;";

        PreparedStatement stmt = conexao.prepareStatement(query);
        int temp = Integer.parseInt(id.toString());
        stmt.setInt(1, temp);
        Usuario usuario = new Usuario();
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            usuario.setId(rs.getLong("id"));
            usuario.setNome(rs.getString("nome"));
            usuario.setEmail(rs.getString("email"));
            usuario.setSexo(rs.getString("sexo"));
            Calendar data = Calendar.getInstance();
            data.setTime(rs.getDate("datanasc"));
            usuario.setDataNascimento(data);
            this.getListaAmigo(usuario);
            this.getListaSolicitacoes(usuario);
            this.getListaRequisicoes(usuario);
            new GrupoDAO().getGrupos(usuario);
            return usuario;
        } else {
            return null;
        }
    }

    /**
     *
     * @param usuario
     * @return TRUE se conseguir gravar a lista de amigos dentro do úsuario
     * passado como parametro. FALSE caso contrário.
     * @throws SQLException
     */
    public boolean getListaAmigo(Usuario usuario) throws SQLException {
        String query = "SELECT DISTINCT e.id_us, e.id_us_amg "
                + "FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR e.id_us_amg = u.id) "
                + "WHERE u.id = ? AND confirm = true;";
        PreparedStatement stmt = conexao.prepareStatement(query);
        stmt.setLong(1, usuario.getId());
        ResultSet rs = stmt.executeQuery();
        List<Long> userUids = new ArrayList<Long>();
        while (rs.next()) {
            if (rs.getLong("id_us") != usuario.getId()
                    || rs.getLong("id_us_amg") != usuario.getId()) {
                if (!userUids.contains(rs.getLong("id_us"))) {
                    userUids.add(rs.getLong("id_us"));
                }
                if (!userUids.contains(rs.getLong("id_us_amg"))) {
                    userUids.add(rs.getLong("id_us_amg"));
                }
            }
        }
        if (!userUids.isEmpty()) {
            usuario.setAmigosId(userUids);
        }

        return false;

    }

    public List<Usuario> getListaAmigo(Usuario usuario, boolean set) throws SQLException {
        String query = "SELECT DISTINCT nome, id"
                + "	FROM ("
                + "	(SELECT DISTINCT e.id_us"
                + "	FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR e.id_us_amg = u.id)"
                + "	WHERE u.id = ? AND confirm = true)"
                + "	UNION"
                + "	(SELECT DISTINCT e.id_us_amg"
                + "	FROM usuario u JOIN e_amigo e ON (u.id = e.id_us OR e.id_us_amg = u.id)"
                + "	WHERE u.id = ? AND confirm = true)) as amigos JOIN usuario us ON (amigos.id_us = us.id);";
        PreparedStatement stmt = conexao.prepareStatement(query);
        stmt.setLong(1, usuario.getId());
        stmt.setLong(2, usuario.getId());
        ResultSet rs = stmt.executeQuery();
        List<Long> userUids = new ArrayList<Long>();
        List<Usuario> usuarios = new ArrayList<Usuario>();

        while (rs.next()) {
            Usuario tus = new Usuario();
            if (usuario.getId() != rs.getLong("id")) {
                tus.setId(rs.getLong("id"));
                tus.setNome(rs.getString("nome"));
                userUids.add(rs.getLong("id"));
                usuarios.add(tus);
            }
        }
        if (!userUids.isEmpty()) {
            if (set) {
                usuario.setAmigosId(userUids);
            } else {
                return usuarios;
            }
        }

        return null;

    }

    /**
     *
     * @return Uma lista com todos usuarios no banco
     * @throws SQLException see final
     */
    public List<Usuario> getLista() throws SQLException, ClassNotFoundException {

        List<Usuario> listaUsuario = new ArrayList<Usuario>();
        PreparedStatement stmt = this.conexao.prepareStatement("select * from usuario;");
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Usuario us = new Usuario();
            us.setId(rs.getLong("id"));
            us.setNome(rs.getString("nome"));
            us.setEmail(rs.getString("email"));
            listaUsuario.add(us);

        }

        rs.close();
        stmt.close();
        return listaUsuario;

    }

    /**
     *
     * @param usuario Instância de usuário a ser adicionado ao banco de dados
     * @return Verdadeiro se o contato foi adicionado com sucesso, falso em
     * outro caso
     * @see final
     */
    public boolean adiciona(Usuario usuario) throws SQLException {
        String sql = "INSERT INTO usuario"
                + "(nome, email, sexo, datanasc, login, senha, avatar)"
                + "values (?,?,?,?,?,md5(?), ?)"
                + "RETURNING id";

        PreparedStatement stmt = conexao.prepareStatement(sql);
        stmt.setString(1, usuario.getNome());
        stmt.setString(2, usuario.getEmail());
        stmt.setString(3, usuario.getSexo());
        //TODO Fazer verificação data != null
        stmt.setDate(4, new Date(usuario.getDataNascimento().getTimeInMillis()));
        stmt.setString(5, usuario.getLogin());
        stmt.setString(6, usuario.getSenha());
        stmt.setString(7, usuario.getAvatar());

        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {
            usuario.setId(rs.getLong("id"));
            stmt.close();
            return true;
        } else {
            stmt.close();
            return false;
        }

    }

    /**
     *
     * @param usuario
     * @return
     * @throws SQLException
     * @since final
     */
    public boolean verificarUsuario(Usuario usuario) throws SQLException, ClassNotFoundException {
        String query =
                "SELECT * "
                + "FROM usuario u "
                + "WHERE u.login=? AND u.senha=md5(?);";

        PreparedStatement stmt = conexao.prepareStatement(query);
        stmt.setString(1, usuario.getLogin());
        stmt.setString(2, usuario.getSenha());

        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {
            usuario.setId(rs.getLong("id"));
            usuario.setNome(rs.getString("nome"));
            usuario.setEmail(rs.getString("email"));
            usuario.setSexo(rs.getString("sexo"));
            Calendar data = Calendar.getInstance();
            data.setTime(rs.getDate("datanasc"));
            usuario.setDataNascimento(data);
            this.getListaAmigo(usuario);
            this.getListaSolicitacoes(usuario);
            this.getListaRequisicoes(usuario);
            new GrupoDAO().getGrupos(usuario);
            return true;
        } else {
            return false;
        }
    }

    public boolean adicionaAmigo(Usuario usuario, Long idAmigo) {
        String query = "INSERT INTO e_amigo (id_us, id_us_amg, confirm) "
                + "VALUES (?,?,?) RETURNING id_us;";
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, usuario.getId());
            stmt.setLong(2, idAmigo);
            stmt.setBoolean(3, false);
            ResultSet rs = stmt.executeQuery();
            stmt.close();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean excluiAmigo(Usuario usuario, Long idAmigo) {
        String query = "DELETE FROM e_amigo"
                + "VALUES (?,?,?) RETURNING id_us;";
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, usuario.getId());
            stmt.setLong(2, idAmigo);
            stmt.setBoolean(3, false);
            ResultSet rs = stmt.executeQuery();
            stmt.close();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean confirmaAmizade(Usuario usuario, Long idAmigo) throws SQLException {
        String query = "UPDATE e_amigo "
                + "SET confirm = true WHERE (id_us = ?) "
                + "AND (id_us_amg=? ) RETURNING id_us;";
        PreparedStatement stmt = conexao.prepareStatement(query);
        stmt.setLong(1, idAmigo);
        stmt.setLong(2, usuario.getId());
        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {
            stmt.close();
            rs.close();
            return true;
        } else {
            stmt.close();
            rs.close();
            return false;
        }


    }

    /**
     * Atribui a lista de solicitacoes que o usuario fez
     *
     * @param usuario
     * @return
     * @throws SQLException
     */
    public boolean getListaSolicitacoes(Usuario usuario) throws SQLException {
        String query = "SELECT DISTINCT e.id_us_amg "
                + "FROM usuario u JOIN e_amigo e ON (u.id = e.id_us) "
                + "WHERE e.confirm = false AND e.id_us = ?;";
        PreparedStatement stmt = conexao.prepareStatement(query);
        stmt.setLong(1, usuario.getId());
        ResultSet rs = stmt.executeQuery();
        List<Long> userUids = new ArrayList<Long>();
        while (rs.next()) {
            userUids.add(rs.getLong("id_us_amg"));
        }
        if (!userUids.isEmpty()) {
            usuario.setRequisitadosId(userUids);
            return true;
        }

        return false;
    }

    /**
     * Atribui no usuario a lista de pedidos de amizade pendente
     *
     * @param usuario
     * @return
     * @throws SQLException
     */
    public boolean getListaRequisicoes(Usuario usuario) throws SQLException {
        String query = "SELECT DISTINCT e.id_us "
                + "FROM e_amigo e "
                + "WHERE e.confirm = false AND e.id_us_amg = ?;";
        PreparedStatement stmt = conexao.prepareStatement(query);
        stmt.setLong(1, usuario.getId());
        ResultSet rs = stmt.executeQuery();
        List<Long> userUids = new ArrayList<Long>();
        while (rs.next()) {
            userUids.add(rs.getLong("id_us"));
        }
        if (!userUids.isEmpty()) {
            usuario.setPedidosPendentes(userUids);
            return true;
        }
        return false;
    }

    public List<Usuario> buscaUsuarios(String busca) throws SQLException {

        List<Usuario> res = new ArrayList<Usuario>();
        String query = "select * from usuario where nome ~* ?;";
        PreparedStatement stmt = conexao.prepareStatement(query);
        String buscaFinal = ".*" + busca.toLowerCase() + ".*";
        stmt.setString(1, buscaFinal);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            Usuario usuario = new Usuario();
            usuario.setId(rs.getLong("id"));
            usuario.setNome(rs.getString("nome"));
            usuario.setEmail(rs.getString("email"));
            usuario.setSexo(rs.getString("sexo"));
            Calendar data = Calendar.getInstance();
            data.setTime(rs.getDate("datanasc"));
            usuario.setDataNascimento(data);
            res.add(usuario);

        }
        return res;
    }
    
}
