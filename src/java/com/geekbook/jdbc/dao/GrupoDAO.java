/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.jdbc.dao;

import com.geekbook.jdbc.ConnectionFactory;
import com.geekbook.jdbc.modelo.Grupo;
import com.geekbook.jdbc.modelo.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pedrotanaka
 *
 */
public class GrupoDAO {

    private Connection conexao;

    public GrupoDAO() throws ClassNotFoundException {
        this.conexao = new ConnectionFactory().getConnection();
    }

    /**
     *
     * @param grupo
     * @param dono
     */
    public boolean adiciona(Grupo grupo, Usuario dono, List<Long> participantesId) {
        String sql = "INSERT INTO grupo"
                + "(nome, id_us_dono, id_us_part)"
                + "values (?, ?, ?)"
                + "RETURNING id";

        PreparedStatement stmt;
        try {

            stmt = conexao.prepareStatement(sql);
            stmt.setString(1, grupo.getNomeGrupo());
            stmt.setLong(2, dono.getId());

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                grupo.setIdGrupo(rs.getLong("id_grupo"));
                stmt.close();
                return true;
            } else {
                stmt.close();
                return false;
            }

        } catch (SQLException ex) {
            Logger.getLogger(GrupoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean adicionaUsuarios(Grupo grupo, List<Long> idUsuarios) throws SQLException {

        String query = "INSERT INTO grupo(id_grupo, id_us_dono, id_us_part, nome_grupo)"
                + "VALUES(?, ?, ?, ?);";
        PreparedStatement stmt = conexao.prepareStatement(query);
        for (Long ids : idUsuarios) {
            stmt.setLong(1, grupo.getIdGrupo());
            stmt.setLong(2, grupo.getIdUsDono());
            stmt.setLong(3, ids);
            stmt.setString(4, grupo.getNomeGrupo());
            stmt.executeUpdate();
        }
        return true;
    }

    public boolean existe(Long idGrupo) throws SQLException {
        String query = "SELECT id_grupo "
                + "FROM grupo"
                + "WHERE id_grupo = ?";
        PreparedStatement st = conexao.prepareStatement(query);
        st.setLong(1, idGrupo);
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            return true;
        }
        return false;
    }

    public Long getNextVal() throws SQLException {
        String query = "SELECT MAX(id_grupo)"
                + "FROM grupo;";
        Statement st = conexao.createStatement();
        ResultSet rs = st.executeQuery(query);
        if (rs.next()) {
            Integer i = rs.getInt("max");
            i++;
            return Long.parseLong(i.toString());
        }
        return null;
    }

    /**
     *
     * @param grupo
     * @param dono
     */
    public boolean excluir(Grupo grupo) throws SQLException {
        String query = "DELETE FROM grupo "
                + "  WHERE ( id_grupo  =  ? AND id_us_dono  =  ? ) RETURNING id_grupo;";
        PreparedStatement stmt = conexao.prepareStatement(query);
        
        int tmp =  Integer.parseInt(grupo.getIdGrupo().toString());
        
        stmt.setInt(1, tmp);
        
        tmp = Integer.parseInt(grupo.getIdUsDono().toString());
        stmt.setLong(2, tmp);
        
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            rs.close();
            return true;
        } else {
            rs.close();
            return false;
        }


    }

    public Grupo getInstanceById(Long id) throws SQLException {
        String query = "SELECT * FROM grupo WHERE id_grupo = ?";
        PreparedStatement pstmt = this.conexao.prepareStatement(query);
        pstmt.setLong(1, id);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            Grupo us = new Grupo();
            us.setNomeGrupo(rs.getString("nome_grupo"));
            us.setIdGrupo(rs.getLong("id_grupo"));
            us.setIdUsDono(rs.getLong("id_us_dono"));
            rs.close();
            pstmt.close();
            return us;
        } else {
            return null;
        }

    }

    /**
     *
     * @param usuario
     * @return
     */
    public boolean editar(Grupo grupo) {
        //TODO consertar aqui. 
        String sql = "UPDATE grupo  "
                + "(nome)  "
                + "VALUES (?)  "
                + "RETURNING id;  ";

        PreparedStatement stmt;
        try {

            stmt = conexao.prepareStatement(sql);
            stmt.setString(1, grupo.getNomeGrupo());

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                stmt.close();                
                return true;
            } else {
                stmt.close();
                return false;
            }

        } catch (SQLException ex) {
            Logger.getLogger(GrupoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }

    public List<Grupo> getGrupos(Usuario usuario) throws SQLException, ClassNotFoundException {
        String query = "SELECT DISTINCT id_grupo, nome_grupo FROM grupo WHERE id_us_dono = ?";
        PreparedStatement pstmt = this.conexao.prepareStatement(query);
        pstmt.setLong(1, usuario.getId());
        ResultSet rs = pstmt.executeQuery();

        List<Grupo> grupos;
        grupos = new ArrayList<Grupo>();
        while (rs.next()) {
            Grupo us = new Grupo();
            us.setIdGrupo(rs.getLong("id_grupo"));
            us.setIdUsDono(usuario.getId());
            us.setNomeGrupo(rs.getString("nome_grupo"));
            grupos.add(us);
        }
        if(!grupos.isEmpty()){
            return grupos;
        }
        
        rs.close();
        pstmt.close();
        return null;
    }

    public List<Usuario> getListaParticipantes(Grupo grupo) {

        String query = "SELECT *   "
                + "FROM grupo g JOIN usuario u ON (g.id_us_part = u.id)   "
                + "WHERE g.id_grupo = ?;";
        
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            int a = Integer.parseInt( grupo.getIdGrupo().toString() );
            stmt.setInt(1, a);
            ResultSet rs = stmt.executeQuery();
            List<Usuario> part;
            part = new ArrayList<Usuario>();
            while (rs.next()) {
                @SuppressWarnings("UnusedAssignment")
                Usuario usTemp = new Usuario();
                usTemp.setId(rs.getLong("id"));
                usTemp.setNome(rs.getString("nome"));
                part.add(usTemp);
            }
            return part;
        } catch (SQLException ex) {
            Logger.getLogger(GrupoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
}
