/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.jdbc.dao;

import com.geekbook.jdbc.ConnectionFactory;
import com.geekbook.jdbc.modelo.Post;
import com.geekbook.jdbc.modelo.Usuario;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pedrotanaka
 */
public class PostDAO {

    private Connection conexao;

    public PostDAO() throws ClassNotFoundException {
        this.conexao = new ConnectionFactory().getConnection();
    }

    /**
     *
     * @param usuario Usuario o qual está postando
     * @param conteudo Texto que será o corpo do post
     * @param anexos Uma <code>String</code> com caminhos dos anexos separados
     * por ':'
     */
    public boolean adicionar(Post post) {
        String sql = "INSERT INTO posts(id_us, conteudo, data_publicacao)  "
                + "VALUES (?, ?, ?) RETURNING id_post;";

        PreparedStatement stmt;
        try {

            stmt = conexao.prepareStatement(sql);
            stmt.setLong(1, post.getIdUsuario());
            stmt.setString(2, post.getConteudo());
            stmt.setDate(3, new Date(post.getDataPublicacao().getTimeInMillis()));

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                post.setIdPost(rs.getLong("id_post"));
                stmt.close();
                if (post.isTemAnexos()) {
                    adicionaImagens(post);
                }
                return true;
            } else {
                stmt.close();
                return false;
            }

        } catch (SQLException ex) {
            Logger.getLogger(GrupoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private void adicionaImagens(Post post) {
        String sql = "INSERT INTO post_multimidia(id_post, imagem)  "
                + "VALUES (?, ?);";
        String caminho = "uploads/post_img/";
        PreparedStatement stmt;
        try {

            stmt = conexao.prepareStatement(sql);
            stmt.setLong(1, post.getIdPost());

            for (String imgName : post.getAnexos()) {
                stmt.setString(2, caminho + imgName);
                stmt.executeUpdate();
            }

        } catch (SQLException ex) {
            Logger.getLogger(GrupoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editar(Post post) {
        String sql = "UPDATE posts   "
                + "SET(conteudo) = (?) WHERE id_post = ?;";

        PreparedStatement stmt;
        try {
            stmt = conexao.prepareStatement(sql);
            stmt.setString(1, post.getConteudo());
            stmt.setLong(2, post.getIdPost());
            stmt.executeUpdate();
            if(post.isTemAnexos()){
                adicionaImagens(post);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GrupoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void excluir(Post post) {
        //TODO consertar aqui. 
        String sql = "DELETE FROM posts "
                + "WHERE id_post =  ?;";

        PreparedStatement stmt;
        try {
            getImagensPost(post);
            stmt = conexao.prepareStatement(sql);
            stmt.setLong(1, post.getIdPost());
            stmt.executeUpdate();
            //TODO Colocar no post a lista de imagens associadas para excluir depois na Servlet
        } catch (SQLException ex) {
            Logger.getLogger(GrupoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Post getInstanceById(Long id){
        String query = "SELECT * FROM posts WHERE id_post = ?;";
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, id);
            ResultSet rs =  stmt.executeQuery();
            if(rs.next()){
                Post p = new Post();
                p.setIdPost(id);
                getImagensPost(p);
                getCurtirPost(p);
                getListaComentario(p);
                p.setConteudo(rs.getString("conteudo"));
                p.setDataPublicacao(rs.getDate("data_publicacao"));
                p.setIdUsuario(rs.getLong("id_us"));
                return p;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        
    }

    public List<Post> getTodosPostsUsuario(Usuario usuario) {
        try {
            String query = "SELECT *  "
                    + "FROM posts   "
                    + "WHERE id_us = ? ORDER BY data_publicacao DESC;";

            List<Post> posts = new ArrayList<Post>();

            PreparedStatement stmt = conexao.prepareStatement(query);

            stmt.setLong(1, usuario.getId());

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Post post = new Post();
                post.setIdPost(rs.getLong("id_post"));
                post.setIdUsuario(usuario.getId());
                post.setConteudo(rs.getString("conteudo"));
                getCurtirPost(post);
                getImagensPost(post);
                getListaComentario(post);
                posts.add(post);
            }
            return posts;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    private  void getCurtirPost(Post post) {
        String query = "SELECT COUNT(*) AS total "
                + " FROM curtir WHERE id_post = ?";
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, post.getIdPost());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                post.setQtdCurtir(rs.getInt("total"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }




    }

    private  void getImagensPost(Post post) {
        String query = "SELECT imagem FROM post_multimidia WHERE id_post = ?";
        PreparedStatement stmt;
        try {
            stmt = conexao.prepareStatement(query);
            stmt.setLong(1, post.getIdPost());
            ResultSet rs = stmt.executeQuery();
            List<String> imgs = new ArrayList<String>();
            while (rs.next()) {
                imgs.add(rs.getString("imagem"));
            }
            post.setAnexos(imgs);
            if (!imgs.isEmpty()) {
                post.setTemAnexos(true);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void comentarPost(Long idPost, Long idUs, String comentario){
        String query = "INSERT INTO comentario (id_post_com, id_us, conteudo_com)   "
                + "VALUES (?, ?, ?);";
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, idPost);
            stmt.setLong(2, idUs);
            stmt.setString(3, comentario);
            stmt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    public boolean curtirPost(Long idPost, Long usId) {
        String query = "SELECT * FROM curtir WHERE id_post = ? AND id_us_curtiu = ? LIMIT 1;";
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, idPost);
            stmt.setLong(2, usId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                query = "INSERT INTO curtir (id_post , id_us_curtiu) VALUES (?, ?);";
                stmt = conexao.prepareStatement(query);
                stmt.setLong(1, idPost);
                stmt.setLong(2, usId);
                stmt.executeUpdate();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    private void getListaComentario(Post post){
        String query = "SELECT * FROM comentario  "
                + "WHERE id_post_com = ?;";
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, post.getIdPost());
            ResultSet rs = stmt.executeQuery();
            List<String> coms = new ArrayList<String>();
            while(rs.next()){
                coms.add(rs.getString("conteudo_com"));
            }
            stmt.close();
            rs.close();
            post.setComentarios(coms);
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public List<Usuario> getTop3Postadores(Long id, Date d1, Date d2) throws ClassNotFoundException{
        String query = "SELECT * FROM get_top3_posters_f(?::INT);";
        UsuarioDAO ud = new UsuarioDAO();
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, id);
//            stmt.setDate(2, d1);
//            stmt.setDate(3, d2);
            ResultSet rs = stmt.executeQuery();
            List<Usuario> usList = new ArrayList<Usuario>();
            while(rs.next()){
                Usuario us = new Usuario();
                us.setId(rs.getLong("id_am"));
                us = ud.getInstanceById(us.getId());
                us.setGrupo(rs.getLong("id_grupo"));
                us.setPost_cont(rs.getInt("total_p"));
                usList.add(us);
            }
            return usList;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        
    }
    
    
    
    public List<Usuario> getTop10inf(Long id, Date d1, Date d2) throws ClassNotFoundException{
        String query = "SELECT * top10_infl_f(?::INT);";
        try {
            PreparedStatement stmt = conexao.prepareStatement(query);
            stmt.setLong(1, id);
            stmt.setDate(2, d1);
            stmt.setDate(3, d2);
            ResultSet rs = stmt.executeQuery();
            UsuarioDAO ud = new UsuarioDAO();
            List<Usuario> usList = new ArrayList<Usuario>();
            while(rs.next()){
                Usuario us = new Usuario();
                us.setId(rs.getLong("id_am"));
                us = ud.getInstanceById(us.getId());
                us.setGrupo(rs.getLong("id_grupo"));
                us.setPost_cont(rs.getInt("total_p"));
                usList.add(us);
            }
            return usList;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
}
