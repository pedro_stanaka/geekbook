/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.ui;

import com.geekbook.jdbc.dao.UsuarioDAO;
import com.geekbook.jdbc.modelo.Usuario;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author pedrotanaka
 */
public class ListaUsuarios {
    UsuarioDAO usDAO;

    public ListaUsuarios() throws ClassNotFoundException, SQLException {
        this.usDAO = new UsuarioDAO();
        
    }
    
    public List<Usuario> getLista() throws SQLException, ClassNotFoundException{
        return this.usDAO.getLista();
    }

}
