/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.ui;

import com.geekbook.jdbc.dao.UsuarioDAO;
import com.geekbook.jdbc.modelo.Usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedrotanaka
 */
public class GetResultadoBusca {
    public List<Usuario> getListaResultante(String busca) throws ClassNotFoundException, SQLException{
        UsuarioDAO uD = new UsuarioDAO();
        List<Usuario> resultado = uD.buscaUsuarios(busca);
        if( resultado != null ){
            return resultado;
        }
        else{
            return null;
        }
    }
}
    