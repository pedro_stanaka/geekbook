/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.servlet;

import com.geekbook.jdbc.dao.GrupoDAO;
import com.geekbook.jdbc.dao.UsuarioDAO;
import com.geekbook.jdbc.modelo.Grupo;
import com.geekbook.jdbc.modelo.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pedrotanaka
 */
public class doGerenciaGrupos extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response@param request servlet request
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        String acao = request.getParameter("acao");
        HttpSession hs = request.getSession(false);
        Usuario us;
        if (hs != null) {
            us = (Usuario) hs.getAttribute("usuario");
            String pagina = "home_logado.jsp";
            if ( acao.equals("listar") ) {
                GrupoDAO gd = new GrupoDAO();
                List<Grupo> grupos = gd.getGrupos(us);
                request.setAttribute("listaGrupos", grupos);
                pagina = "gerencia_grupos.jsp";
                
            } else if(acao.equals("participantes")){
                String nomeGrupo;
                GrupoDAO gd = new GrupoDAO();
                Long idGrupo = Long.parseLong(request.getParameter("id"));
                Grupo gr = gd.getInstanceById(idGrupo);
                nomeGrupo = gr.getNomeGrupo();
                
                List<Usuario> listPart = gd.getListaParticipantes(gr);
                
                request.setAttribute("listaParticipantes", listPart);
                request.setAttribute("nomeGrupo", nomeGrupo);
                
                pagina = "grupo_view.jsp";
            } else if(acao.equals("preparaAdicionar")){
                UsuarioDAO ud = new UsuarioDAO();
                List<Usuario> list;
                list = ud.getListaAmigo(us,false);
                request.setAttribute("listaAmigos", list);
                pagina = "cadastro_grupo.jsp";
            }else if(acao.equals("adicionar")){
                
                Grupo g = new Grupo();
                GrupoDAO gd = new GrupoDAO();
                
                String[] idsAmigos;
                Long[] idL;
                
                idsAmigos = request.getParameterValues("usuarios");
                
                String nomeGrupo = request.getParameter("nomeGrupo");
                
                
                idL = new Long[idsAmigos.length];
                for (int i = 0; i < idsAmigos.length; i++) {
                     idL[i] = Long.parseLong(idsAmigos[i]);
                }
                
                
                g.setIdUsDono(us.getId());
                g.setNomeGrupo(nomeGrupo);
                Long nextId = gd.getNextVal();
                g.setIdGrupo(nextId);
                List<Long> l = Arrays.asList(idL);
                if(gd.adicionaUsuarios(g, l)){
                    pagina = "doGerenciaGrupos?acao=listar";
                }
            } else if(acao.equals("excluir")){
                GrupoDAO gd = new GrupoDAO();
                Grupo g = new Grupo();
                
                Long id = Long.parseLong(request.getParameter("id"));
                g.setIdGrupo(id);
                g.setIdUsDono(us.getId());
                gd.excluir(g);
                
            }
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doGerenciaGrupos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(doGerenciaGrupos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doGerenciaGrupos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(doGerenciaGrupos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
