/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.servlet;

import com.geekbook.jdbc.dao.PostDAO;
import com.geekbook.jdbc.modelo.Post;
import com.geekbook.jdbc.modelo.Usuario;
import com.geekbook.utils.MD5;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author pedrotanaka
 */
public class doGerenciaPosts extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");


        Usuario usuario;
        HttpSession hs = request.getSession(false);
        String pagina = "doCarregaHome";
        if (hs != null) {
            usuario = (Usuario) hs.getAttribute("usuario");
            PostDAO pd = new PostDAO();
            // <editor-fold defaultstate="collapsed" desc="Acao = postar">
            if (request.getParameter("acao").equals("postar")) {
                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(4096);
                //pasta que vai salvar os arquivos temporarios - /home/pedrotanaka/NetBeansProjects/GeekBook/web/img
                factory.setRepository(new File("/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/temp/"));
                ServletFileUpload upload = new ServletFileUpload(factory);
                upload.setSizeMax(15000000);
                List<FileItem> fileItems;
                List<String> uploadsFile = new ArrayList<String>();
                Post post = new Post();
                post.setTemAnexos(false);
                try {
                    fileItems = upload.parseRequest(request);
                    for (FileItem fi : fileItems) {
                        if (fi.isFormField()) {
                            String conteudo = fi.getString();
                            post.setConteudo(conteudo);
                        } else {
                            if (fi.getSize() > 50) {

                                Long dt = new Date().getTime();
                                String fileName = new MD5().MD5(dt.toString()) + ".png";
                                uploadsFile.add(fileName);
                                try {
                                    fi.write(new File("/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/post_img/", fileName));
                                } catch (Exception ex) {
                                    Logger.getLogger(doUploadFoto.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                FileChannel srcChannel = new FileInputStream(
                                        "/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/post_img/"
                                        + fileName).getChannel();
                                FileChannel dstChannel = new FileOutputStream(
                                        "/home/pedrotanaka/NetBeansProjects/GeekBook/web/uploads/post_img/"
                                        + fileName).getChannel();

                                dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
                                // Close the channels  
                                srcChannel.close();
                                dstChannel.close();
                            }
                        }

                    }

                    if (!uploadsFile.isEmpty()) {
                        post.setTemAnexos(true);
                        post.setAnexos(uploadsFile);
                    }
                    post.setIdUsuario(usuario.getId());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    post.setDataPublicacao(cal);

                    pd = new PostDAO();
                    pd.adicionar(post);


                } catch (FileUploadException ex) {
                    Logger.getLogger(doGerenciaPosts.class.getName()).log(Level.SEVERE, null, ex);
                }



            } //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Acao = curtir">
            else if (request.getParameter("acao").equals("curtir")) {
                pd.curtirPost(Long.parseLong(request.getParameter("idPost")), usuario.getId());
            } //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Acao =pre-editar">
            else if (request.getParameter("acao").equals("pre-editar")) {
                Post p = new PostDAO().getInstanceById(Long.parseLong(request.getParameter("id_post")));
                request.setAttribute("post", p);
                pagina = "edit_post.jsp";
            } 
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Acao=editar">
            else if(request.getParameter("acao").equals("editar")){
                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(4096);
                //pasta que vai salvar os arquivos temporarios - /home/pedrotanaka/NetBeansProjects/GeekBook/web/img
                factory.setRepository(new File("/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/temp/"));
                ServletFileUpload upload = new ServletFileUpload(factory);
                upload.setSizeMax(15000000);
                List<FileItem> fileItems;
                List<String> uploadsFile = new ArrayList<String>();
                Post post = new Post();
                post.setTemAnexos(false);
                try {
                    fileItems = upload.parseRequest(request);
                    for (FileItem fi : fileItems) {
                        if (fi.isFormField()) {
                            String conteudo = fi.getString();
                            post.setConteudo(conteudo);
                        } else {
                            if (fi.getSize() > 50) {

                                Long dt = new Date().getTime();
                                String fileName = new MD5().MD5(dt.toString()) + ".png";
                                uploadsFile.add(fileName);
                                try {
                                    fi.write(new File("/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/post_img/", fileName));
                                } catch (Exception ex) {
                                    Logger.getLogger(doUploadFoto.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                FileChannel srcChannel = new FileInputStream(
                                        "/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/post_img/"
                                        + fileName).getChannel();
                                FileChannel dstChannel = new FileOutputStream(
                                        "/home/pedrotanaka/NetBeansProjects/GeekBook/web/uploads/post_img/"
                                        + fileName).getChannel();
                                dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
                                // Close the channels  
                                srcChannel.close();
                                dstChannel.close();
                            }
                        }
                    }

                    if (!uploadsFile.isEmpty()) {
                        post.setTemAnexos(true);
                        post.setAnexos(uploadsFile);
                    }
                    post.setIdUsuario(usuario.getId());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    post.setDataPublicacao(cal);
                    post.setIdPost(Long.parseLong(request.getParameter("post_id")));
                    pd = new PostDAO();
                    pd.editar(post);


                } catch (FileUploadException ex) {
                    Logger.getLogger(doGerenciaPosts.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Acao = excluir">
            else if (request.getParameter("acao").equals("excluir")) {
                Post p = new Post();
                p.setIdPost(Long.parseLong(request.getParameter("id_post")));
                pd.excluir(p);
                if (!p.getAnexos().isEmpty()) {
                    for (String string : p.getAnexos()) {
                        File f = new File("/home/pedrotanaka/NetBeansProjects/GeekBook/web/" + string);
                        if (f.exists()) {
                            f.delete();
                        }
                        File f2 = new File("/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/" + string);
                        if (f2.exists()) {
                            f2.delete();
                        }
                    }
                }
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Acao=comentar">
            else if(request.getParameter("acao").equals("comentar")){
                String comentario = request.getParameter("comentario");
                if(comentario !=  null && comentario.length() > 1 ){
                    Long id = Long.parseLong(request.getParameter("id_post"));
                    pd.comentarPost(id, usuario.getId(), comentario);
                }
            }
            // </editor-fold>
            
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doGerenciaPosts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doGerenciaPosts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
