/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.servlet;

import com.geekbook.jdbc.dao.UsuarioDAO;
import com.geekbook.jdbc.modelo.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pedrotanaka
 */
public class doLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        
        String pagina = "index.jsp?erro=1";
        Usuario us;
        us = new Usuario();
        us.setLogin(request.getParameter("login"));
        us.setSenha(request.getParameter("senha"));

        if (new UsuarioDAO().verificarUsuario(us)) {
            HttpSession sessao = request.getSession();
            sessao.setAttribute("usuario", us);
            pagina = "doCarregaHome";

        }
        response.sendRedirect(pagina);

        
        
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);


            } catch (SQLException ex) {
                Logger.getLogger(doLogin.class
                        .getName()).log(Level.SEVERE, null, ex);
            }


        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doLogin.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);


            } catch (SQLException ex) {
                Logger.getLogger(doLogin.class
                        .getName()).log(Level.SEVERE, null, ex);
            }


        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doLogin.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
