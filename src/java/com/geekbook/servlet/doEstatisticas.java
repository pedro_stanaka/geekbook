/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.geekbook.servlet;

import com.geekbook.jdbc.dao.PostDAO;
import com.geekbook.jdbc.dao.UsuarioDAO;
import com.geekbook.jdbc.modelo.Usuario;
import com.sun.xml.internal.ws.util.xml.CDATA;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pedrotanaka
 */
public class doEstatisticas extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        String pagina = "index.jsp";
        HttpSession hs = request.getSession(false);
        if (hs != null) {
            Usuario us = (Usuario) hs.getAttribute("usuario");
            PostDAO pd = new PostDAO();
            UsuarioDAO ud = new UsuarioDAO();
            if (request.getParameter("acao").equals("listar")) {
                try {
                    List<Usuario> listaAmigos = ud.getListaAmigo(us, false);
                    request.setAttribute("listaAmigos", listaAmigos);
                    pagina = "gerencia_estatistica.jsp";
                } catch (SQLException ex) {
                    Logger.getLogger(doEstatisticas.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (request.getParameter("acao").equals("estatistica")) {
                String data_com = request.getParameter("data-com");
                String data_fim = request.getParameter("data-fim");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Long amigo = Long.parseLong(request.getParameter("amigo"));
                if (data_com != null && data_fim != null) {
                    try {
                        Date dc = sdf.parse(data_com);
                        Date df = sdf.parse(data_fim);
                        List<Usuario> top3 = pd.getTop3Postadores(us.getId(), new java.sql.Date(dc.getTime()), new java.sql.Date(df.getTime()));
                        System.out.println("Tamanho!!  "+top3.size()+"\n\n\n\n");
                        request.setAttribute("top3", top3);
                        pagina = "show_est.jsp";
                    } catch (ParseException pEx) {
                        Logger.getLogger(doCadastroUsuario.class.getName()).log(Level.SEVERE, null, pEx);
                    }
                }
            }
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doEstatisticas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doEstatisticas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
