package com.geekbook.servlet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.geekbook.jdbc.dao.UsuarioDAO;
import com.geekbook.jdbc.modelo.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author pedrotanaka
 */
public class doBusca extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, SQLException, IOException {
        //Notei que a linha que estava aqui setando o conteudo da request podia fazer o setAttribute
        //nao funcionar, por isso tirei!
        try {
            String busca = (String) request.getParameter("texto_busca");
            UsuarioDAO usDAO;
            usDAO = new UsuarioDAO();
            List<Usuario> result = usDAO.buscaUsuarios(busca);
            request.setAttribute("resultList", result);
            RequestDispatcher rd = request.getRequestDispatcher("resultado_busca.jsp");
            rd.forward(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(doBusca.class.getName()).log(Level.SEVERE, null, ex);
        }
        //For more info:
        //http://theopentutorials.com/examples/java-ee/jsp/pagination-in-servlet-and-jsp/
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            RequestDispatcher rd = request.getRequestDispatcher("resultado_busca.jsp");
            rd.forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(doBusca.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(doBusca.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet de busca no sistema GB";
    }// </editor-fold>
}
