package com.geekbook.servlet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.geekbook.jdbc.modelo.Usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author pedrotanaka
 */
public class doUploadFoto extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Usuario usuario;
        HttpSession hs = request.getSession(false);
        if (hs != null) {
            usuario = (Usuario) hs.getAttribute("usuario");

            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(4096);
            //pasta que vai salvar os arquivos temporarios - /home/pedrotanaka/NetBeansProjects/GeekBook/web/img
            factory.setRepository(new File("/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/temp/"));
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(1000000);
            List fileItems;
            try {
                fileItems = upload.parseRequest(request);
                Iterator i = fileItems.iterator();

                FileItem fi = (FileItem) i.next();
                String fileName = usuario.getId().toString() + ".png";
                try {
                    fi.write(new File("/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/avatar/", fileName));
                } catch (Exception ex) {
                    Logger.getLogger(doUploadFoto.class.getName()).log(Level.SEVERE, null, ex);
                }
                FileChannel srcChannel = new FileInputStream(
                        "/home/pedrotanaka/NetBeansProjects/GeekBook/build/web/uploads/avatar/"
                        + usuario.getId().toString() + ".png").getChannel();
                FileChannel dstChannel = new FileOutputStream(
                        "/home/pedrotanaka/NetBeansProjects/GeekBook/web/uploads/avatar/"
                        + usuario.getId().toString()
                        + ".png").getChannel();

                dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
                // Close the channels  
                srcChannel.close();
                dstChannel.close();
            } catch (FileUploadException ex) {
                Logger.getLogger(doUploadFoto.class.getName()).log(Level.SEVERE, null, ex);
            }
            RequestDispatcher rd = request.getRequestDispatcher("doCarregarPerfil");
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(doUploadFoto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(doUploadFoto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
