<%-- 
    Document   : erro_msg
    Created on : 20/08/2012, 14:36:03
    Author     : pedrotanaka
--%>

<%@tag description="Tag para mostrar erros" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="e_type" required="true"%>
<% %>
<%-- any content can be specified here e.g.: --%>
<h2>${message}</h2>