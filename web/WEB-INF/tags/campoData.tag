<%-- 
    Document   : campoData
    Created on : 20/08/2012, 12:56:07
    Author     : pedrotanaka
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="id" required="true"%>
<%@attribute name="value" required="false"%>
<%@attribute name="max_date" required="false" %>

<input type="text" id="${id}" name="${id}" value="${value}"/>
<script type="text/javascript">
 $(document).ready(function(){
         $("#${id}").datepicker({
            dateFormat: 'dd/mm/yy',
            dayNames: [
            'Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'
            ],
            dayNamesMin: [
            'D','S','T','Q','Q','S','S','D'
            ],
            dayNamesShort: [
            'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'
            ],
            monthNames: [
            'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',
            'Outubro','Novembro','Dezembro'
            ],
            monthNamesShort: [
            'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set',
            'Out','Nov','Dez'
            ],
            nextText: 'Próximo',
            prevText: 'Anterior',
            maxDate: "${max_date}"
            
        });
 });

</script>