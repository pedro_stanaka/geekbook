<%-- 
    Document   : link_img_preview
    Created on : 13/10/2012, 14:47:24
    Author     : pedrotanaka
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="href" required="true"%>
<%@attribute name="content" required="true"%>


<%-- any content can be specified here e.g.: --%>
<a href="${href}" class="preview"  target="_blank">${content}</a>
<script>
    $('.preview').imgPreview({
    containerID: 'imgPreviewWithStyles',
    imgCSS: {
        // Limit preview size:
        height: 200
    },
    // When container is shown:
    onShow: function(link){
        // Animate link:
        $(link).stop().animate({opacity:0.4});
        // Reset image:
        $('img', this).stop().css({opacity:0});
    },
    // When image has loaded:
    onLoad: function(){
        // Animate image
        $(this).animate({opacity:1}, 300);
    },
    // When container hides: 
    onHide: function(link){
        // Animate link:
        $(link).stop().animate({opacity:1});
    }
});
</script>