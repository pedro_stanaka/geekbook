<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${sessionScope.usuario ne null}">
    <jsp:useBean id="uiMostraUsuario" scope="page" class="com.geekbook.ui.ListaUsuarios"/>
<!DOCTYPE html>
<html>
    <head>
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/jquery-ui-1.8.22.custom.min.js"></script>
        <script src="js/main-script.js.js"></script>
        <link rel="stylesheet" type="text/css" href="css/default.css"/>
        <link rel="stylesheet" type="text/css" href="css/excite-bike/jquery-ui-1.8.22.custom.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Lista Pendentes ::</title>
    </head>
    <body>
        <div class="info_usuario">
            <div class="usuario_nome">
                Nome completo
            </div>
            <div class="usuario_email">
                <p>Email</p>
            </div>
        </div>
        <c:forEach var="usuario" items="${uiMostraUsuario.lista}">
            <c:if test="${sessionScope.usuario.isRequisitado(usuario.id) eq true}">
                    <div class="info_usuario">
                        <div class="usuario_nome">
                            <a href="usuario_load.jsp?id_us=${usuario.id}">${usuario.nome}</a>
                        </div>
                        <div class="usuario_email">
                            <p><a href="mailto:${usuario.email}">${usuario.email}</a></p>
                        </div>
                    </div>
            </c:if>
        </c:forEach>
    </body>
</html>
</c:when>
<c:otherwise>
    <jsp:forward page="index.jsp"/>
</c:otherwise>
</c:choose>

