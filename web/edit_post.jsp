<%-- 
    Document   : edit_post
    Created on : 15/10/2012, 12:37:52
    Author     : pedrotanaka
--%>
<%@taglib prefix="gb" tagdir="/WEB-INF/tags/" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="includes/header.jsp"></jsp:include>
        <title>Editar Post</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h2>Edição de Post</h2>
            </div>
            <div id="content">
                <div id="new_post">
                    <form name="new_post" action="doGerenciaPosts?acao=editar&post_id=${post.idPost}" method="post" id="form_post" enctype="multipart/form-data" accept-charset="UTF-8">
                        <textarea id="textarea_post" form="form_post" name="conteudo" rows="8" cols="40" id="texto">${post.conteudo}</textarea>
                        <br/>
                        <gb:fileList id="lista_fotos" id_input="fotos_input" name="fotos"></gb:fileList>
                        <br/>
                        <input class="botao_sub" type="submit" value="Editar Post" />
                    </form>                        
                </div>
            </div>
        </div>
    </body>
</html>
