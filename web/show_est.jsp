<%-- 
    Document   : gerencia_estatistica
    Created on : 22/10/2012, 00:32:03
    Author     : pedrotanaka
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="gb" tagdir="/WEB-INF/tags/"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${sessionScope.usuario ne null}">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
        <script src="js/main-script.js.js"></script>
        <link rel="stylesheet" type="text/css" href="css/trontastic/jquery-ui-1.8.23.custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/default.css"/>
        <title>Estatisticas</title>
    </head>
    <body>
        <div id="container">
            <div id="header"></div>
            <div id="busca_main">
                <form name="busca_usuario" method="post" action="doBusca">
                    <input class="entrada_texto" type="text" name="texto_busca"/>
                    <input class="botao_sub" type="submit" value="Buscar"/>
                </form>
                <a style="display: inline; float: right" href="doLogout">Logout</a>
            </div>
            <div class="est_1">
                <h2>Top 3 posters por grupo</h2>
                <c:forEach items="${top3}" var="us">
                    <div class="post">
                        Nome: ${us.nome}<br/>
                        Grupo: ${us.grupo}
                    </div>
                </c:forEach>
            </div>
            <a href="home_logado.jsp" >Voltar</a>
        </div>
    </body>
</html>
    </c:when>
    <c:otherwise>
        <jsp:forward page="home_logado.jsp"></jsp:forward>
    </c:otherwise>
</c:choose>