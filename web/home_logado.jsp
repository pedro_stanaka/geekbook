<%-- 
    Document   : home_logado
    Created on : 13/08/2012, 14:59:33
    Author     : pedrotanaka
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="gb" tagdir="/WEB-INF/tags/"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${ListaPosts eq null}">
        <jsp:forward page="doCarregaHome"></jsp:forward>
    </c:when>
    <c:when test="${ sessionScope.usuario ne null}">

        <!DOCTYPE html>
        <html>
            <head>
                <jsp:include page="includes/header.jsp"></jsp:include>
                <title>:: ${sessionScope.usuario.nome} ::</title>
            </head>
            <body>
                <div id="container">
                    <div id="header"></div>
                    <div id="busca_main">
                        <form name="busca_usuario" method="post" action="doBusca">
                            <input class="entrada_texto" type="text" name="texto_busca"/>
                            <input class="botao_sub" type="submit" value="Buscar"/>
                        </form>
                        <a style="display: inline; float: right" href="doLogout">Logout</a>
                    </div>
                    <div id="foto">
                        <div id="avatar_div">
                            <img src="uploads/avatar/${sessionScope.usuario.id}.png"/>
                        </div>
                        <a href="cadastro_avatar.jsp">Mude sua foto</a><br/>
                        <a href="editar_perfil.jsp">Editar seu perfil</a>
                    </div>
                    <h1 style="color: white"> <c:out value="Bem vindo ${sessionScope.usuario.nome}"></c:out> </h1>
                        <ul>
                            <li><a href="lista_usuarios.jsp">Lista de usuários</a></li>
                            <li><a href="lista_amigos.jsp">Lista de amigos</a></li>
                            <li><a href="doGerenciaGrupos?acao=listar">Grupos</a></li>
                            <li><a href="lista_pendente.jsp">Solicitações Pendentes</a></li>
                            <li><a href="lista_pedidos.jsp">Pedidos a aceitar</a></li>
                            <li><a href="doSugestAmigos">Sugestões de amizade</a></li>
                            <li id="stalker"><a href="doEstatisticas?acao=listar">Entrar no modo stalker</a></li>
                        </ul>
                        <div id="posts_div">
                            <div id="new_post">
                                <form name="new_post" action="doGerenciaPosts?acao=postar" method="post" id="form_post" enctype="multipart/form-data" accept-charset="UTF-8">
                                    <textarea id="textarea_post" form="form_post" name="conteudo" rows="8" cols="40" id="texto" placeholder="What's going on?"></textarea>
                                    <br/>
                                <gb:fileList id="lista_fotos" id_input="fotos_input" name="fotos"></gb:fileList>
                                    <br/>
                                    <input class="botao_sub" type="submit" value="Novo Post" />
                                </form>
                            </div>
                            <div id="show_posts" >
                            <c:forEach  items="${ListaPosts}" var="post">
                                <div class="post">
                                    <a href="doGerenciaPosts?acao=excluir&id_post=${post.idPost}"><div class="excluir_post"></div></a>
                                    <a href="doGerenciaPosts?acao=pre-editar&id_post=${post.idPost}"><div class="editar_post"></div></a>
                                    <div class="texto_post">
                                        ${post.conteudo}
                                    </div>
                                    <c:if test="${post.temAnexos}">
                                        <div class="img_post">
                                            <c:forEach items="${post.anexos}" var="img">
                                                <a href="${img}" class="prev">Foto</a><br/>
                                            </c:forEach>
                                        </div>
                                    </c:if>
                                    <div class="comenta_post">
                                        <form action="doGerenciaPosts?acao=comentar&id_post=${post.idPost}" method="post" accept-charset="utf-8" name="form_comentario">
                                            <input type="text" name="comentario"/>
                                            <input type="submit" value="Comentar"/>
                                        </form>
                                        <div class="show_comentario">
                                            <c:forEach items="${post.comentarios}" var="com">
                                                <div class="comenta_conteudo">
                                                    ${com}
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="info_post">
                                        <a href="doGerenciaPosts?acao=curtir&idPost=${post.idPost}">
                                            <div class="curtir_div"></div></a>
                                        <div class="qtd_curtir_div">
                                            ${post.qtdCurtir} geeks curtiram isto.
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>

            </body>

        </c:when>

        <c:otherwise>
            <jsp:forward page="index.jsp" ></jsp:forward>
        </c:otherwise>
    </c:choose>
</html>

