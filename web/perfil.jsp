<%-- 
    Document   : home_logado
    Created on : 13/08/2012, 14:59:33
    Author     : pedrotanaka
--%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.geekbook.jdbc.modelo.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:choose>
<c:when test="${ sessionScope.usuario ne null}">
<!DOCTYPE html>
<html>
    <head>
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/jquery-ui-1.8.22.custom.min.js"></script>
        <script src="js/main-script.js.js"></script>
        <link rel="stylesheet" type="text/css" href="css/default.css"/>
        <link rel="stylesheet" type="text/css" href="css/excite-bike/jquery-ui-1.8.22.custom.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: ${usuario.nome} ::</title>
    </head>
    <body>
        <div id="container">
            <div id="header"></div>
            <div style="width: 800px; height: auto">
                <img src="uploads/avatar/${usuario.id}.png"/>
                <a href="cadastro_avatar.jsp">Mude sua foto do perfil</a>
            </div>
            <h1 style="color: white"> <c:out value="${usuario.nome}"></c:out> </h1>
            <h3 style="color: white"> 
                <c:choose>
                    <c:when test="${usuario.sexo eq 'M'}">
                        Sexo: Masculino
                    </c:when>
                    <c:when test="${usuario.sexo eq 'F'}">
                        Sexo: Feminino
                    </c:when>
                </c:choose>
            </h3>
                 
                <a href="doLogout">Logout</a>
                <div id="busca_main">
                    <form name="busca_usuario" method="post" action="doBusca">
                        <input type="text" name="texto_busca"/>
                        <input type="submit" value="Buscar"/>
                    </form>
                </div>
            </div>
    </body>
</c:when>
    <c:otherwise>
        <jsp:forward page="index.jsp" ></jsp:forward>
    </c:otherwise>
</c:choose>
</html>

