<%-- 
    Document   : cadastroUsuario
    Created on : 06/08/2012, 10:57:59
    Author     : pedrotanaka
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="gb" tagdir="/WEB-INF/tags/"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
        <script src="js/main-script.js.js"></script>
        <link rel="stylesheet" type="text/css" href="css/trontastic/jquery-ui-1.8.23.custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/default.css"/>
        <title>Cadasto Usuario</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
            </div>
            <div id="content">
                <h1>Cadastrar Novo Usuário</h1>
                <form name="formAdicionarGrupo" method="post" action="doGerenciaGrupos?acao=adicionar">
                    Nome do grupo: <input name="nomeGrupo" type="text" size="20" /><br/>

                    <p style="color: red">Amigos a serem incluídos:(obrigatório, ao menos um)</p>
                    <ul>
                        <c:forEach var="usuario" items="${listaAmigos}">
                            <li> <input type="checkbox" name="usuarios" value="${usuario.id}" width="3px" height="3px"/>  ${usuario.nome}</li>
                        </c:forEach>
                    </ul>
                    <input type="submit" value="Adicionar grupo"/>
                    <input type="reset" value="Limpar"/>
                </form>
                <p><a href="index.jsp">Voltar</p>
                <div>
                    <p style="color: red;">
                        <c:if test="${not empty erro}">
                            <c:choose>
                                <c:when test="${erro eq 'grupoDup'}">
                                    <c:out value="Você já tem um grupo com esse nome!">
                                    </c:out>
                                </c:when>
                                <c:when test="${erro eq 'vazio'}">
                                    <c:out value="Você deixou campos vazios"></c:out>
                                </c:when>
                            </c:choose>
                        </c:if>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
