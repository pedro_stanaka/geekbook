<%-- 
    Document   : cadastro_avatar
    Created on : 10/09/2012, 08:55:16
    Author     : pedrotanaka
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="includes/header.jsp"></jsp:include>
        <title>:: Foto ${sessionScope.usuario.nome} ::</title>
    </head>
    <body>
        <div id="header"></div>
        <div>
          
        <h1>Mude sua foto</h1>
        <fieldset>

        <form name="formCadastroFoto" method="post" action="doUploadFoto" enctype="multipart/form-data">
            <input type="file" name="file" size="50" />
            <br />
            <input type="submit" value="Upload File" />
        </form>

        <a href="index.jsp">Voltar</a>
        </fieldset>   

        </div>    
    </body>
</html>