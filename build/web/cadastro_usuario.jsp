<%-- 
    Document   : cadastroUsuario
    Created on : 06/08/2012, 10:57:59
    Author     : pedrotanaka
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="gb" tagdir="/WEB-INF/tags/"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
        <script src="js/main-script.js.js"></script>
        <link rel="stylesheet" type="text/css" href="css/trontastic/jquery-ui-1.8.23.custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/default.css"/>
        <title>Cadasto Usuario</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
            </div>
            <div id="content">
                <h1>Cadastrar Novo Usuário</h1>
                <form name="formCadastroUsuario" method="post" action="cadastrar">
                    Nome: <input name="nome" type="text" size="20" ><br/>
                    Email: <input name="email" type="text" size="20" /><br/>
                    Sexo:  <input type="radio" name="sexo" value="M" checked="checked" /> Masculino
                    <input type="radio" name="sexo" value="F"> Feminino<br/>
                    Data Nascimento(dd-mm-aaaa): <gb:campoData id="data_nasc"></gb:campoData>
                    <!--<input type="text" id="data_nasc" name="data_nasc"/>-->
                        Login: <input name="login" type="text" size="20" ><br/>
                        Senha: <input name="senha" type="password" size="20" ><br/>
                        <input type="submit" value="Cadastrar"/>
                        <input type="reset" value="Limpar"/>
                    </form>
                    <p><a href="index.jsp">Voltar</p>
                    <div >
                        <p style="color: red;">
                            <c:if test="${not empty erro}">
                                <c:if test="${erro eq 'loginDup'}">
                                    <c:out value="Este login já está em uso.\nPor favor, tente outro login!">
                                    </c:out>
                                </c:if>
                            </c:if>
                        </p>
                    </div>
                </div>
            </div>
        </body>
    </html>
