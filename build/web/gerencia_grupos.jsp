<%-- 
    Document   : gerencia_grupos
    Created on : 13/08/2012, 14:59:33
    Author     : pedrotanaka
--%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.geekbook.jdbc.modelo.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${ sessionScope.usuario ne null}">

        <!DOCTYPE html>
        <html>
            <head>
                <script src="js/jquery-1.7.2.min.js"></script>
                <script src="js/jquery-ui-1.8.22.custom.min.js"></script>
                <script src="js/main-script.js.js"></script>
                <link rel="stylesheet" type="text/css" href="css/default.css"/>
                <link rel="stylesheet" type="text/css" href="css/excite-bike/jquery-ui-1.8.22.custom.css"/>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>:: Grupos ::</title>
            </head>
            <body>
                <div id="container">
                    <div id="header"></div>
                    <ul>
                        <c:forEach var="grupo" items="${listaGrupos}">
                            <li>
                                <a href="doGerenciaGrupos?acao=participantes&&id=${grupo.idGrupo}">${grupo.nomeGrupo}</a>
                                <a class="menu_link" href="doGerenciaGrupos?acao=excluir&&id=${grupo.idGrupo}">Excluir</a>
                            </li>
                        </c:forEach>
                    </ul>

                    
                    <a class="menu_link" href="doGerenciaGrupos?acao=preparaAdicionar">Novo Grupo</a>
                    <a href="doLogout">Logout</a>
                    <a href="home_logado.jsp" style="display: inline-block">Voltar</a>
                    <div id="busca_main">
                        <h3> Busca de usuários </h3>
                        <form name="busca_usuario" method="POST" action="doBusca">
                            <input type="text" name="texto_busca"/>
                            <input type="submit" value="Buscar"/>
                        </form>
                    </div>
                </div>
            </body>
        </c:when>
        <c:otherwise>
            <jsp:forward page="index.jsp" ></jsp:forward>
        </c:otherwise>
    </c:choose>
</html>

