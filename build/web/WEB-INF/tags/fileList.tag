<%-- 
    Document   : fileList
    Created on : 05/10/2012, 13:24:27
    Author     : pedrotanaka
--%>

<%@tag description="Lista de arquivos para inputs com multiple" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="id" required="true"%>
<%@attribute name="id_input" required="true"%>
<%@attribute name="name"  required="true" %>


<input type="file" name="${name}" id="${id_input}" multiple="" onchange="makeFileList();">
<ul id="${id}">
    <li>Nenhum item selecionado</li>
</ul>
<script type="text/javascript">
    function makeFileList() {
        var input = document.getElementById("${id_input}");
        var ul = document.getElementById("${id}");
        while (ul.hasChildNodes()) {
            ul.removeChild(ul.firstChild);
        }
        for (var i = 0; i < input.files.length; i++) {
            var li = document.createElement("li");
            li.innerHTML = input.files[i].name;
            ul.appendChild(li);
        }
        if(!ul.hasChildNodes()) {
            var li = document.createElement("li");
            li.innerHTML = 'No Files Selected';
            ul.appendChild(li);
        }
    }
</script>